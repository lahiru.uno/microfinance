// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "./UserIdentity.sol";

contract LoanPayment{
    
    enum PaymentState { HANDSHAKE_START, HANDSHAKE_SUCCESS, MONEY_HANDEDOVER, MONEY_RECEIVED, FLAG_TRANSACTION, CANCEL }
    
    struct Payment{
        uint id;
        uint pin;
        address borrower;
        address broker;
        uint loanId;
        uint tokenAmount;
        uint amount;
        PaymentState status;
    }
    
    event paymentHandshake(
        uint id,
        uint pin,
        address borrower,
        address broker,
        uint loanId,
        uint tokenAmount,
        uint amount,
        PaymentState status
    );
    
    event paymentHandshakeConfirm(
        uint id,
        PaymentState status
    );
    
    event moneyHandover(
        uint id,
        PaymentState status
    );
    
    event moneyReceived(
        uint id,
        PaymentState status
    );
    
    event paymentCancel(
        uint id,
        PaymentState status,
        string reason
    );
    
    event flagPayment(
        uint id,
        PaymentState status,
        string reason
    );
    
    modifier isBroker(address _broker){
        require(identitySC.verifyIsBroker(_broker), 'Broker Only');
        _;
    }
    
    modifier isPaymentIn(uint _paymentId, PaymentState _state){
        bool isValid = false;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId && payments[i].status == _state)
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }
    
    modifier checkPin(uint _paymentId, uint _pin){
        bool isValid = false;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId && payments[i].pin == _pin)
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }
    
    modifier checkBroker(uint _paymentId, address _broker){
        bool isValid = false;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId && payments[i].broker == _broker)
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }
    
    modifier checkBorrower(uint _paymentId, address _borrower){
        bool isValid = false;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId && payments[i].borrower == _borrower)
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }
    
    address private admin;
    UserIdentity identitySC;
    Payment[] private payments;
    
    constructor (address _identitySC) {
        admin = msg.sender;
        identitySC = UserIdentity(_identitySC);
    }
    
    function startHandshake(uint _loanId, uint _amount, uint _tokens, address _broker, uint _pin) public isBroker(_broker)
    {
        Payment memory p = Payment(payments.length + 1, _pin, msg.sender, _broker, _loanId, _tokens, _amount, PaymentState.HANDSHAKE_START);
        
        payments.push(p);
        
        emit paymentHandshake(p.id, p.pin, p.borrower, p.broker, p.loanId, p.tokenAmount, p.amount, p.status);
    }
    
    function confirmHandshake(uint _paymentId, uint _pin) public 
        isBroker(msg.sender) isPaymentIn(_paymentId, PaymentState.HANDSHAKE_START) checkPin(_paymentId, _pin) checkBroker(_paymentId, msg.sender)
    {
        uint x = 0;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId)
            {
                payments[i].status = PaymentState.HANDSHAKE_SUCCESS;
                x = i;
                break;
            }
        }
        
        emit paymentHandshakeConfirm(payments[x].id, payments[x].status);
    }
    
    function handoverMoney(uint _paymentId) public 
        checkBorrower(_paymentId, msg.sender) isPaymentIn(_paymentId, PaymentState.HANDSHAKE_SUCCESS)
    {
        uint x = 0;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId)
            {
                payments[i].status = PaymentState.MONEY_HANDEDOVER;
                x = i;
                break;
            }
        }
        
        emit moneyHandover(payments[x].id, payments[x].status);
    }
    
    function confirmMoneyReceived(uint _paymentId) public 
        checkBroker(_paymentId, msg.sender) isPaymentIn(_paymentId, PaymentState.MONEY_HANDEDOVER)
    {
        uint x = 0;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId)
            {
                payments[i].status = PaymentState.MONEY_RECEIVED;
                x = i;
                break;
            }
        }
        
        emit moneyReceived(payments[x].id, payments[x].status);
    }
    
    function cancelByBorrower(uint _paymentId) public 
        checkBorrower(_paymentId, msg.sender) isPaymentIn(_paymentId, PaymentState.HANDSHAKE_SUCCESS)
    {
        uint x = 0;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId)
            {
                payments[i].status = PaymentState.CANCEL;
                x = i;
                break;
            }
        }
        
        emit paymentCancel(payments[x].id, payments[x].status, "Payment was canceled by Borrower");
    }
    
    function cancelByBroker(uint _paymentId) public 
        checkBroker(_paymentId, msg.sender) isPaymentIn(_paymentId, PaymentState.HANDSHAKE_START)
    {
        uint x = 0;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId)
            {
                payments[i].status = PaymentState.CANCEL;
                x = i;
                break;
            }
        }
        
        emit paymentCancel(payments[x].id, payments[x].status, "Payment was canceled by Broker");
    }
    
    function flagByBorrower(uint _paymentId) public 
        checkBorrower(_paymentId, msg.sender) isPaymentIn(_paymentId, PaymentState.MONEY_HANDEDOVER)
    {
        uint x = 0;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId)
            {
                payments[i].status = PaymentState.FLAG_TRANSACTION;
                x = i;
                break;
            }
        }
        
        emit flagPayment(payments[x].id, payments[x].status, "Payment was flagged by Borrower");
    }
    
    function flagByBroker(uint _paymentId) public 
        checkBroker(_paymentId, msg.sender) isPaymentIn(_paymentId, PaymentState.MONEY_HANDEDOVER)
    {
        uint x = 0;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId)
            {
                payments[i].status = PaymentState.FLAG_TRANSACTION;
                x = i;
                break;
            }
        }
        
        emit flagPayment(payments[x].id, payments[x].status, "Payment was flagged by Broker");
    }

    function getAllPayments() public view returns (Payment[] memory)
    {
        return payments;
    }
}
