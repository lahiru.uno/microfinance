// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract UserIdentity{
    
    enum Role { BROKER, BORROWER, INSURER }
    enum UserState { PENDING, APPROVED, REJECTED }
    
    struct User{
        uint id; 
        string socialSecurityId; // each property has an unique social security id
        address userAddress;
        string name;
        string documentsUri;
        Role role;
        bool isBankApproved;
        UserState state;
    }

    struct Borrower{
        uint id; 
        string socialSecurityId; // each property has an unique social security id
        address userAddress;
        string name;
        string documentsUri;
        Role role;
        address addedBy;
        bool isBankApproved;
        UserState state;
    }
    
    event newBrokerAdded(
        uint id,
        string socialSecurityId,
        address userAddress,
        string name,
        string documentsUri,
        Role role,
        bool isBankApproved,
        UserState state
    );
    
    event newBorrowerAdded(
        uint id, 
        string socialSecurityId,
        address userAddress,
        string name,
        string documentsUri,
        Role role,
        address addedBy,
        bool isBankApproved,
        UserState state
    );
    
    event newInsurerAdded(
        uint id, 
        string socialSecurityId,
        address userAddress,
        string name,
        string documentsUri,
        Role role,
        bool isBankApproved,
        UserState state
    );
    
    address owner;
    
    uint brokersCount = 0;
    uint borrowersCount = 0;
    uint insurersCount = 0;
    
    // public access mdifier for testing purposes only.
    mapping(address => Borrower) public borrowers;
    mapping(address => User) public brokers;
    mapping(address => User) public insurers;
    
    address[] private brokersAddresses;
    address[] private borrowersAddresses;
    address[] private insurersAddresses;
    
    constructor()
    {
        owner = msg.sender;
    }
    
    modifier isOwner(address _address)
    {
        require(owner == _address, 'Bank Only');
        _;
    }
    
    modifier isBroker(address _address)
    {
        require(brokers[_address].role == Role.BROKER, 'Broker Only');
        _;
    }
    
    modifier isNewBroker(address _address)
    {
        bool isNew = false;
        if (brokers[_address].id == 0)
        {
            isNew = true;
        }
        require(isNew, 'Broker already exists');
        _;
    }
    
    modifier isNewIsnsurer(address _address)
    {
        bool isNew = false;
        if (insurers[_address].id == 0)
        {
            isNew = true;
        }
        require(isNew, 'Insurer already exists');
        _;
    }
    
    modifier isNewBorrower(address _address)
    {
        bool isNew = false;
        if (borrowers[_address].id == 0)
        {
            isNew = true;
        }
        require(isNew, 'Borrower already exists');
        _;
    }
    
    function addBroker(string memory _socialSecurityId, address _address, string memory _name, string memory _documentsUri) public isNewBroker(_address)
    {
        brokersCount = brokersCount + 1;
        User memory user = User(brokersCount, _socialSecurityId, _address, _name, _documentsUri, Role.BROKER, false, UserState.PENDING);
        brokers[_address] = user;
        brokersAddresses.push(_address);
        emit newBrokerAdded(user.id, user.socialSecurityId, user.userAddress, user.name, user.documentsUri, user.role, user.isBankApproved, user.state);
    }
    
    function addInsurer(string memory _socialSecurityId, address _address, string memory _name, string memory _documentsUri) public isNewIsnsurer(_address)
    {
        insurersCount = insurersCount + 1;
        User memory user = User(insurersCount, _socialSecurityId, _address, _name, _documentsUri, Role.INSURER, false, UserState.PENDING);
        insurers[_address] = user;
        insurersAddresses.push(_address);
        emit newInsurerAdded(user.id, user.socialSecurityId, user.userAddress, user.name, user.documentsUri, user.role, user.isBankApproved, user.state);
    }
    
    function addBorrower(string memory _socialSecurityId, address _address, string memory _name, string memory _documentsUri) public isBroker(msg.sender) isNewBorrower(_address)
    {
        borrowersCount = borrowersCount + 1;
        Borrower memory user = Borrower(borrowersCount, _socialSecurityId, _address, _name, _documentsUri,  Role.BORROWER, msg.sender, false, UserState.PENDING);
        borrowers[_address] = user;
        borrowersAddresses.push(_address);
        emit newBorrowerAdded(user.id, user.socialSecurityId, user.userAddress, user.name, user.documentsUri, user.role, user.addedBy, user.isBankApproved, user.state);
    }
    
    function verifyIsBroker(address _address) public view returns(bool)
    {
        bool isValid = false;
        isValid = brokers[_address].role == Role.BROKER && brokers[_address].isBankApproved == true;
        return isValid;
    }
    
    function verifyIsBorrower(address _address) public view returns(bool)
    {
        bool isValid = false;
        isValid = borrowers[_address].role == Role.BORROWER && borrowers[_address].isBankApproved == true;
        return isValid;
    }
    
    function verifyIsInsurer(address _address) public view returns(bool)
    {
        bool isValid = false;
        isValid = insurers[_address].role == Role.INSURER && insurers[_address].isBankApproved == true;
        return isValid;
    }

    function verifyIsBank(address _address) public view returns(bool)
    {
        return owner == _address;
    }
    
    function approveBorrower(address _address) public isOwner(msg.sender)
    {
        borrowers[_address].isBankApproved = true;
        borrowers[_address].state = UserState.APPROVED;
    }
    
    function approveBroker(address _address) public isOwner(msg.sender)
    {
        brokers[_address].isBankApproved = true;
        brokers[_address].state = UserState.APPROVED;
    }
    
    function approveInsuranceCompany(address _address) public isOwner(msg.sender)
    {
        insurers[_address].isBankApproved = true;
        insurers[_address].state = UserState.APPROVED;
    }

    function rejectBorrower(address _address) public isOwner(msg.sender)
    {
        borrowers[_address].isBankApproved = false;
        borrowers[_address].state = UserState.REJECTED;
    }
    
    function rejectBroker(address _address) public isOwner(msg.sender)
    {
        brokers[_address].isBankApproved = false;
        brokers[_address].state = UserState.REJECTED;
    }
    
    function rejectInsuranceCompany(address _address) public isOwner(msg.sender)
    {
        insurers[_address].isBankApproved = false;
        insurers[_address].state = UserState.REJECTED;
    }
    
    function getAllBrokers() public view returns (User[] memory){
        User[] memory ret = new User[](brokersCount);
        for (uint i = 0; i < brokersCount; i++) {
            ret[i] = brokers[brokersAddresses[i]];
        }
        return ret;
    }
    
    function getAllInsurers() public view returns (User[] memory){
        User[] memory ret = new User[](insurersCount);
        for (uint i = 0; i < insurersCount; i++) {
            ret[i] = insurers[insurersAddresses[i]];
        }
        return ret;
    }
    
    function getAllBorrowers() public view returns (Borrower[] memory){
        Borrower[] memory ret = new Borrower[](borrowersCount);
        for (uint i = 0; i < borrowersCount; i++) {
            ret[i] = borrowers[borrowersAddresses[i]];
        }
        return ret;
    }

    //Add modifier to check user State before change it.
    
}