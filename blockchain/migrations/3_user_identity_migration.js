const UserIdentity = artifacts.require("UserIdentity");
const BankLoan = artifacts.require("BankLoan");
const InsurancePolicy = artifacts.require("InsurancePolicy");
const LoanPayment = artifacts.require("LoanPayment")

// module.exports = function(deployer, network, accounts) {
//   deployer.deploy(UserIdentity).then(
//     function(){
//       return deployer.deploy(BankLoan, UserIdentity.address);
//     }
//   ).then(
//     function(){
//       return deployer.deploy(InsurancePolicy, UserIdentity.address, {from: accounts[1]});
//     }
//   ).then(
//     function(){
//       return deployer.deploy(LoanPayment, UserIdentity.address);
//     }
//   )
// };

module.exports = async function(deployer, network, accounts) {
  await deployer.deploy(UserIdentity);
  const userIdentityInstance = await UserIdentity.deployed();

  console.log(userIdentityInstance.address);

  await deployer.deploy(BankLoan, userIdentityInstance.address);
  await deployer.deploy(InsurancePolicy, userIdentityInstance.address, {from: accounts[3]});
  await deployer.deploy(LoanPayment, userIdentityInstance.address);

};