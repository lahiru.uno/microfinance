const Express = require('express')
const mongoose = require('mongoose')
const cors = require('cors');
const IPFS = require('ipfs-api')

const InputDataDecoder = require('ethereum-input-data-decoder');

// var multer  = require('multer')
const fileupload = require("express-fileupload");
const swaggerUi = require('swagger-ui-express')
const swaggerJsDoc = require('swagger-jsdoc')

const ipfs = new IPFS({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' });

//SWAGGER
const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Bank API",
            version: "1.0.0",
            description: "Bank API for Microfinance"
        },
        servers: [
            {
                url: "http://localhost:9091"
            }
        ],
    },
    apis: ["./routes/*.js"]
}

const specs = swaggerJsDoc(options)

const app = Express()

//Import Routed
const plansRoute = require('./routes/plans');
const paymentsRoute = require('./routes/payments');
const transactionRoute = require('./routes/transaction');
const swaggerJSDoc = require('swagger-jsdoc')

//MIDDLEWARE
app.use(cors())

app.use(Express.urlencoded({ extended: true }));
app.use(Express.json())

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(specs))

app.use('/loan-plans', plansRoute);
app.use('/loan-payments', paymentsRoute);
app.use('/decode-transaction', transactionRoute);
app.use('/upload', fileupload());

//ROUTES
app.get('/', (req, res) => {
    res.send('Welcome to Bank Server')
})

app.post('/upload', async (req, res) => {
    console.log(req.files.file.name)
	console.log("react to post action - loadFile");
	var logFile = req.files.file.name;

	console.log(logFile);
	var buffer = req.files.file.data;
	console.log(buffer);

	ipfs.files.add(buffer, (err, result) => {
		if(err){
			console.log(err);
			res.status(500).send('Faild to upload to IPFS');
		}
		console.log(result[0].hash);
		res.send(result[0].hash);
	})
})

//Connect to DB
const url = 'mongodb://127.0.0.1:27017/bank-db'
mongoose.connect(
    url,
    { useNewUrlParser: true },
    () => {
        console.log('connected to Bank DB')
    })


//LISTENING
app.listen(9091)
