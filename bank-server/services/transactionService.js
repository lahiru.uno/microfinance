const InputDataDecoder = require('ethereum-input-data-decoder');
const BankLoanArtifact = require('../abis/BankLoan.json');
const UserIdentityArtifact = require('../abis/UserIdentity.json');
const MicroTokenArtifact = require('../abis/MicroToken.json');
const LoanPaymentArtifact = require('../abis/LoanPayment.json');


const decodeBankLoanData = (req) => {

	const decoder = new InputDataDecoder(BankLoanArtifact.abi);
	const data = '0x9daafc6c00000000000000000000000000000000000000000000000000000000000003e8000000000000000000000000000000000000000000000000000000000000000c000000000000000000000000000000000000000000000000000000000000000a00000000000000000000000000000000000000000000000000000000000000a0000000000000000000000000d24a27ba895e41ce8384fa80dd63fdf1a8a381d600000000000000000000000000000000000000000000000000000000000000013100000000000000000000000000000000000000000000000000000000000000';
	const result = decoder.decodeData(req.params.input);

	const inputs = []
	const types = result.types
	for (let i = 0; i < types.length; i++) {
		if (types[i] === 'uint256'){
			inputs.push(result.inputs[i].toString())
		}
		else  {
			inputs.push(result.inputs[i])
		}
	}

	result.inputs = inputs;

	console.log(result);
	console.log(result.inputs[0].toString())
	console.log(result.inputs[1].toString())
	console.log(result.inputs[2].toString())

	return result
}

const decodeUserIdentityData = (req) => {

	const decoder = new InputDataDecoder(UserIdentityArtifact.abi);
	const result = decoder.decodeData(req.params.input);

	const inputs = []
	const types = result.types
	for (let i = 0; i < types.length; i++) {
		if (types[i] === 'uint256'){
			inputs.push(result.inputs[i].toString())
		}
		else  {
			inputs.push(result.inputs[i])
		}
	}

	result.inputs = inputs;

	return result
}

const decodeMicroTokenData = (req) => {

	const decoder = new InputDataDecoder(MicroTokenArtifact.abi);
	const result = decoder.decodeData(req.params.input);

	const inputs = []
	const types = result.types
	for (let i = 0; i < types.length; i++) {
		if (types[i] === 'uint256'){
			inputs.push(result.inputs[i].toString())
		}
		else  {
			inputs.push(result.inputs[i])
		}
	}

	result.inputs = inputs;

	return result
}

const decodeLoanPaymentData = (req) => {

	const decoder = new InputDataDecoder(LoanPaymentArtifact.abi);
	const result = decoder.decodeData(req.params.input);

	const inputs = []
	const types = result.types
	for (let i = 0; i < types.length; i++) {
		if (types[i] === 'uint256'){
			inputs.push(result.inputs[i].toString())
		}
		else  {
			inputs.push(result.inputs[i])
		}
	}

	result.inputs = inputs;

	return result
}

const transactionService = {
	decodeBankLoanTransaction: async (req) => {
		return decodeBankLoanData(req);
	},
	decodeUserIdentityTransaction: async (req) => {
		return decodeUserIdentityData(req);
	},
	decodeMicroTokenTransaction: async (req) => {
		return decodeMicroTokenData(req);
	},
	decodeLoanPaymentTransaction: async (req) => {
		return decodeLoanPaymentData(req);
	}

}
  
module.exports = transactionService;