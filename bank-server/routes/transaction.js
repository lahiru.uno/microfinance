const Express = require('express')
const router = Express.Router()

const transactionService = require('../services/transactionService');

//GET ALL Loan Payments
/**
 * @swagger
 * components:
 *  schemas:
 *      LoanPayment:
 *          type: object
 *          required:
 *              -borrower
 *              -loanId
 *              -amount
 *              -transactionHash
 *          properties:
 *              _id:
 *                  type: string
 *                  description: The auto generated id from Mongo DB
 *              borrower:
 *                  type: string
 *                  description: Borrower address
 *              loanId:
 *                  type: number
 *                  description: Loan id
 *              amount:
 *                  type: number
 *                  description: Paid amount
 *              transactionHash:
 *                  type: sring
 *                  description: Blockchain transaction hash
 *          example:
 *              _id: 60d6ffbcc743bb4d6c69da68
 *              borrower: '0x940028a249EB48446dA6E68DD9A1927Cd4822A9f'
 *              loanId: 1
 *              amount: 300
 *              transactionHash: '0x0025a9562a86021ec187a33d6c3ad65a5ee3538b52e383769fec675fd500387d'
 *              
 */


/**
 * @swagger
 * tags:
 *  name: Loan Payments
 *  description: The Bank Loan Payment API for the Microfinance
 */

/**
 * @swagger
 * /decode-transaction/bank-loan/{input}:
 *  get:
 *      summary: Returns the list of all loan payment transactions
 *      tags: [Loan Payments]
 *      responses:
 *          200:
 *              description: The list of the loan plans
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items: 
 *                              $ref: '#/components/schemas/LoanPayment'
 */
router.get('/bank-loan/:input', async (req, res) => {
    try {
		const transactionData = await transactionService.decodeBankLoanTransaction(req);
        console.log(req)
		res.json(transactionData);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

/**
 * @swagger
 * /decode-transaction/user-identity/{input}:
 *  get:
 *      summary: Returns the list of all loan payment transactions
 *      tags: [Loan Payments]
 *      responses:
 *          200:
 *              description: The list of the loan plans
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items: 
 *                              $ref: '#/components/schemas/LoanPayment'
 */
 router.get('/user-identity/:input', async (req, res) => {
    try {
		const transactionData = await transactionService.decodeUserIdentityTransaction(req);
        console.log(req)
		res.json(transactionData);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

/**
 * @swagger
 * /decode-transaction/user-identity/{input}:
 *  get:
 *      summary: Returns the list of all loan payment transactions
 *      tags: [Loan Payments]
 *      responses:
 *          200:
 *              description: The list of the loan plans
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items: 
 *                              $ref: '#/components/schemas/LoanPayment'
 */
 router.get('/micro-token/:input', async (req, res) => {
    try {
		const transactionData = await transactionService.decodeMicroTokenTransaction(req);
        console.log(req)
		res.json(transactionData);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

/**
 * @swagger
 * /decode-transaction/user-identity/{input}:
 *  get:
 *      summary: Returns the list of all loan payment transactions
 *      tags: [Loan Payments]
 *      responses:
 *          200:
 *              description: The list of the loan plans
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items: 
 *                              $ref: '#/components/schemas/LoanPayment'
 */
 router.get('/loan-payment/:input', async (req, res) => {
    try {
		const transactionData = await transactionService.decodeLoanPaymentTransaction(req);
        console.log(req)
		res.json(transactionData);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

module.exports = router;
