Deploy Smart Contract to the Ganache
====================================

This section describes how to deploy your smart contracts into the Ganache private Ethereum blockchain. 
First, you have to make sure you are running Ganache locally on your machine.
Then we can use Truffle to deploy smart contracts into the Ganache local blockchain.
The solidity version is already configured in the ``truffle-config.js`` file.

Run Ganache
-----------

Once you've installed Ganache, you should see the following screen whenever you open it and choose 
the **Quickstart** option:

.. image:: ../images/ganache_accounts.png

In this window, you can see 10 wallet accounts addresses with ``100ETH`` in each which can be used for testing purposes.
Also, you can find the ``MNEMONIC`` and ``RPC SERVER`` address which will be use later in this project.

Deploy Smart Contracts using Truffle
-------------------------------------

We can execute the following command on the terminal in the project root directory to 
deploy the all 5 smart-contracts into the Ganache Etherum blockchain 
which will be running on network ``127.0.0.1`` port ``7545`` . 
Before executing the following command make sure that your Ganache Ethereum blockchain is 
running on your machine. ::

    truffle migrate --reset

we can use the ``--reset`` option to run all migrations from the beginning instead of running from the 
last complete migration. Following details will print on the terminal while deploying the smart contracts.

.. image:: ../images/truffle_migration.png

After successfully deploy all the contracts in to the blockchain it will print following on the terminal.

.. image:: ../images/truffle_migration_summary.png