.. _project-structure-target:

Project structure
=================

Here is the folder structure of Microfinance system. This system consists of 5 projects as shown below. ::

    Microfinance
    |--bank-server
    |--bank-web-app
    |--blockchain
    |--docs
    |--insurance-server
    |--insurance-web-app


* ``bank-server/``: Node Server project for Bank.
* ``bank-web-app/``: React web application for Bank.
* ``blockchain/``: Truffle project for develop and deploy smart contracts
* ``docs/``: Directory for Sphinx documnets.
* ``insurance-server/``: Node Server application for Insurance Company.
* ``insurance-web-app/``: React web application for Insurance Company.

Deployment Diagram
-------------------

.. image:: ../images/deployment.png

As shown in the diagram the Bank and the Insurance Company have web applications developed using React.
These Bank and Insurance Company web applications connect to the Bank Server and the Insurance Server respectively.
These React web applciation to Node Server connection uses HTTP.
These servers consist of NodeJs HTTP server component and a Mongo DB database component. 
Both the Bank and the Insurance Company web applications connect to blockchain using Web3. 
The Bank Server connects to IPFS using Infura and HTTPS.
