Run Bank Server Component
=========================

Bank server was developed using NodeJs. 
This is a HTTP server and handles the loan plans related functionalities.

Prerequisites
-------------

Before start the Bank server run Mongo DB in background.
Bank server will connect to local Mongo DB running on port ``27017`` default port for the Mongo DB.
In bank server project Mongo DB configures as follows. ::

    mongodb://127.0.0.1:27017/bank-db

This code segment will connects the bank server to Mongo DB and ``bank-db`` database. 
If your Mongo DB running on a different host or port you can config it in line number 90 of ``index.js`` file in ``bank-server`` directory.
You can change the db name by replacing the ``bank-db`` in the above code snippet.

Start Bank Server
-----------------

To run the Bank server change to ``bank-server`` directry and run the follwoing command on the terminal. ::

    npm start

Check Bank Server Availability
------------------------------

After successfully executes the above code you can check bank server availablity by navigate to ``localhost:9091`` in your browser.
Default setting will run the bank server on port ``9091``. 
If your port is used by another process in your machine you can run the bank server on different port by replacing 
the ``9091`` on line number 100 in ``index.js`` file.

It will print the following information on your browser when you navigate to ``localhost:9091``.
If you change the host or port please use the neccassary changes to the URL and check.

.. image:: ../images/bank_server_root.png

Swagger API Documentation
-------------------------

To view the API documentation yoou can navigate to ``localhost:9091/api-docs``.
You can view the following information on your browser.

.. image:: ../images/swagger_bank.png