Overview of Bank Web Application
================================

This section describes the important functions of the ``Bank Web Application``.
**Bank Web Application** developed using ``Next.js``.
It's resides inside the ``bank-web-app`` directory.

Project Structure
------------------

Here is the structure of the Bank Web Application. ::

    bank-web-app
    |--components
    |--node_modules
    |--pages
    |--public
    |--stores
    |--styles
    |--package.json

* ``components`` - Directory for the application components.
* ``node_modules`` - Directory of node modules.
* ``pages`` - Directory of pages with dynamic rotes.
* ``public`` - Directory of static files.
* ``stores`` - Directory of Application Context files.
* ``styles`` - Default style files.
* ``package.json`` - Project dependencies and configurations.

Project Dependencies
--------------------

Here are the project dependencies defined inside the ``package.json`` file. ::

    "dependencies": {
        "antd": "^4.16.13",
        "ipfs-api": "^26.1.2",
        "moment": "^2.29.1",
        "next": "11.1.0",
        "react": "17.0.2",
        "react-dom": "17.0.2",
        "react-moment": "^1.1.1",
        "web3": "^1.5.2"
    },

* ``antd`` - Ant design dependency. Ant design is a enterprice-class UI design language and React UI design library.
* ``ipfs-api`` - Dependecy to interact with the IPFS.
* ``next`` - Application framework.
* ``react`` - React dependency.
* ``react-dom`` - React DOM dependecy.
* ``react-moment`` - Process time related data in the application.
* ``web3`` - Dependecny to interact with Ethereum blockchain smart contracts.

Prerequisites
-------------

1. Blockchain - Before run the Bank Web Application we have to deploy all the contracts to the Ethereum blockchain 
and configure smart contract related data(contract addresses and abi) within the application.

2. Bank Web Server - Bank Web Application connects to its web server. It's better to connects the Web Apllication to active
Bank Web Server. If the Bank Web Server running in a different host or post config them properly in the files.

