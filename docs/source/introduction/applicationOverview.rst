
Application Overview
=====================

The Microfinance project is about deploying new ERC20 token and use as a alternative currency type. 
We assume that these tokens were use by non-banking people. 
This project has 4 main user roles.

* ``Bank``: Bank personals who is responsible in ERC20 operations
* ``Borrower`` : The non-banking people who is getting tokens as loans from the Bank.
* ``Broker`` : The role who is connecting Bank and Borrowers.
* ``Insurance Company`` : Insure the token loans.

This project is consists of 5 projects. 

* Bank web application
* Bank serve application
* Insurance Company web application
* Insurance Company server application
* Blockchain (Truffle project for smart contract development)

These projects use different technologies to fullfil the different requirements.
These applications were owned by the Bank and the Insurance Company and used by stakeholders mentioned above.
These project are depending on each other. The following architecture diagram shows the connections between these projects.

Layered Architecture
--------------------

Layered architecture diagram of the system.

.. image:: ../images/layered-architecture.png


In this system we use 5 smart contracts deployed in to the blockchain. 
These smart contracts were accessed by both Bank web application and Insurance Company web application.
The Bank web application communicates with both Bank server and the blockchain. 
Bank server connects to the InterPlanetary File System(IPFS) which is used to store user documents.
Like Bank web application, Insurance Company web application communicates with both Insurance Company server and the blockchain.


In the following sections we discuss how to run these projects, their dependencies, and functioanlities in detail.