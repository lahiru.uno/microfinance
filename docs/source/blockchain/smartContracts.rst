Smart contracts
===============

In this project we use 5 smart contracts for different requirements. 
More details about these smart contracts are discussed in following sections.
All these smart contracts are resides in the ``contracts`` directory. ::

    Microfinance
        |--blockchain
        |  |--contracts
        |  |  |--BankLoan.sol
        |  |  |--InsurancePolicy.sol
        |  |  |--LoanPayment.sol
        |  |  |--MicroToken.sol
        |  |  |--Migrations.sol
        |  |  |--UserIdentity.sol
        |  |--migrations
        |  |--test
        |  |--truffle-config.js


Micro Token smart contract - MicroToken.sol
-------------------------------------------

This smart contract handles the ERC20 token in the system. All the ERC20 token related information like 
Token name, total supply, and functionalities like transfer tokens, account balances, were handle through this
``MicroToken`` smart contract. ``MicroToken`` smart contract implements the ``IERC20`` smart contract interface.


Attributes
~~~~~~~~~~

``MicroToken`` smart contract has token related attributes. 

::

    string public constant symbol = "MICT";
    string public constant name = "Microfinance Token";
    uint8 public constant decimals = 2;
    uint private constant __totalSupply = 100000;
    mapping (address => uint) private __balanceOf;
    mapping (address => mapping (address => uint)) private __allowances;

* ``symbol``: Symbol of the ERC20. This symbol will show in wallets and other places.
* ``name``: We can set a name for the token. In this project our token name is ``Microfinance Token``
* ``decimals``: This attribute configs the number of decimal places we use in our token. We set it to 2. All the token values use upto 2 decimal values
* ``__totalSupply``: This attribute holds the total number of tokens planned to use in the system.
* ``__balanceOf``: This map contains the token balance of each account.
* ``__allowances``: Account owner can grant permission to another account to do the token transfers on behalf of them. But owner can grant permission limited token amount. This mapping holds the number of tokens allowed to transfer from one account to another by third party account.

Constructor
~~~~~~~~~~~

The following code segment shows the ``constructor`` of the MicroToken smart contract. ::

    constructor() {
            __balanceOf[msg.sender] = __totalSupply;
    }

This constructor function will assign all the tokens(``__totalSupply``) to the smart contract deployer's(``msg.sender``) account.
In our system the Bank will get the total token amount at the begining.
So This smart contract should deploy via Bank's account.

Functions
~~~~~~~~~

In this section we discuss about the ``MicroToken`` smart contract functionalities.
Since ``MicroToken.sol`` contract implements the ``IERC20`` interface form **OpenZeppelin** you can find more about these functions 
form `github IERC20 repo <https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/IERC20.sol>`_.

totalSupply
^^^^^^^^^^^

This function returns the ``__totalSupply`` of the token. 
This is a public function. ::

    function totalSupply() public pure override returns (uint _totalSupply) { 
        _totalSupply = __totalSupply;
    }

balanceOf
^^^^^^^^^

This function returns the token balance of given account. Account address should pass as a parameter to this function. 
This is a public function. ::

    function balanceOf(address _addr) public view override returns (uint balance) {
        return __balanceOf[_addr];
    }

transfer
^^^^^^^^

This function transfer tokens from one account to another. 
Token will transfer caller's account. ``_to`` is the receivers account and ``_value `` is the token amount. 
This function returns true if transfer successfull and false otherwise. ::

    function transfer(address _to, uint _value) public override returns (bool success) {
        if (_value > 0 && _value <= balanceOf(msg.sender)) {
            __balanceOf[msg.sender] -= _value;
            __balanceOf[_to] += _value;
            return true;
    }
        return false;
    }

transferFrom
^^^^^^^^^^^^

This functions transfer tokens from one account to another by thirdparty account. 
Before transfer the tokens it may check some conditions. 
Sender account address pass as the ``_from`` parameter and receiver's account address pass as the ``_to`` parameter.
Token amount pass as the ``_value`` parameter to this functions.
It will return ``true`` if successfull or ``false`` otherwise. ::

    function transferFrom(address _from, address _to, uint _value) public override returns (bool success) {
        if (__allowances[_from][msg.sender] > 0 &&
            _value > 0 &&
            __allowances[_from][msg.sender] >= _value &&
            __balanceOf[_from] >= _value) {
            __balanceOf[_from] -= _value;
            __balanceOf[_to] += _value;
                __allowances[_from][msg.sender] -= _value;
            return true;
    }
        return false;
    }


approve
^^^^^^^

Owner can grant permission to transfer some tokens from his account by sender. 
To grant permission owner should ``approve`` the spender's account address ``_spender`` and the token amount ``_value``.
This function returns ``true`` if it is successfull. ::

    function approve(address _spender, uint _value) public override returns (bool success) {
        __allowances[msg.sender][_spender] = _value;
        return true;
    }

allowance
^^^^^^^^^

This function returns the remaining token allowance from ``_owner`` account to ``_spender``, 
the two account addresses pass as the parameters for this function. ::

    function allowance(address _owner, address _spender) public view override returns (uint remaining) {
            return __allowances[_owner][_spender];
    }

User Identity Smart contract - UserIdentity.sol
------------------------------------------------

This contract holds the User details of the Microfinance system. It will register all Broker, Borrower and Insurance Co. details.
In this section we discuss about ``UserIdentity.sol`` in detail.

ENUM
~~~~

1. Role - This enum holds user role values of the system. There 3 user roles in the system. ::

    enum Role { PUBLIC, BROKER, BORROWER, VENDOR, INSURER }
    
2. UserState - This enum holds the user states. There are 3 states of user in the system. ::

    enum UserState { PENDING, APPROVED, REJECTED }

* ``PENDING`` - Newly registered user. 
* ``APPROVED`` - Bank approved user.
* ``REJECTED`` - Bank rejected user.

Structs
~~~~~~~

1. User - This struct holds the user attributes. ::

    struct User{
        uint id; 
        string socialSecurityId; // each property has an unique social security id
        address userAddress;
        string name;
        string documentsUri;
        Role role;
        bool isBankApproved;
        UserState state;
    }

2. Borrower - This struct holds the Borrower attributes. ::

    struct Borrower{
        uint id; 
        string socialSecurityId; // each property has an unique social security id
        address userAddress;
        string name;
        string documentsUri;
        Role role;
        address addedBy;
        bool isBankApproved;
        UserState state;
    }

* ``id`` - System assigned id number for the user. This is an incremental number.
* ``socialSecurityId`` - This attribute holds the social security.
* ``userAddress`` - User account address. Acount address from Ganache.
* ``name`` - User name
* ``documentsUri`` - IPFS uri of user documents.
*  ``role`` - User role(BROKER, BORROWER, INSURER)
* ``addedBy`` - The Broker address of who registered the Borrower.
* ``isBankApproved`` - Bank approval.
* ``state`` - State of the user.

Modifiers
~~~~~~~~~

The following modifiers are used in the ``UserIdentity.sol`` functions.

* ``isOwner`` - Checks the function caller is the admin of the contract.
* ``isBroker`` - Checks the function caller is a Broker.
* ``isNewBroker(_broker)`` - Checks the ``_broker`` is a new Broker.
* ``isNewIsnsurer(_insurer)`` - Checks the ``_insurer`` is a new Insurer.
* ``isNewBorrower(_borrower)`` _ Checks the ``_borrower`` is a new Borrower.

Attributes
~~~~~~~~~~

``UserIdentity.sol`` contains the following attributes. 

* ``owner`` - Holds the deployer account address of the smart contract. 
* ``brokersCount`` - Holds the total brokers in the system. 
* ``borrowersCount`` - Holds the total borrowers in the system.
* ``insurersCount`` - Holds the total Insurers in the system.
    
* ``borrowers`` - This mapping holds all the borrowers details in the system. (address to Borrower mapping)
* ``brokers`` - This mapping holds all the brokers details in the system. (address to User mapping)
* ``insurers`` - This mapping holds all the Insurers details in the system. (address to User mapping)
    
* ``brokersAddresses`` - This array contains all the brokers addresses.
* ``borrowersAddresses`` - This array contains all the borrowers addresses.
* ``insurersAddresses`` - This array contains all the insurers addresses.

Constructor
~~~~~~~~~~~

The constructor will assign the owner address as the contract deployer(``msg.sender``) address. ::

    constructor()
    {
        owner = msg.sender;
    }


Functions
~~~~~~~~~

addBroker
^^^^^^^^^

This function adds the new broker account to the system. ::

    function addBroker(string memory _socialSecurityId, address _address, string memory _name, string memory _documentsUri) public isNewBroker(_address)

Parameters:
    * _socialSecurityId - Social Security ID of the Broker.
    * _address - Account address of the Broker.
    * _name - Broker name.
    * _documentsUri - IPFS hash value of the Broker documents.

This function will create a new Broker object with role=BROKER and state=PENDING and map it to the account address.
Before this function executes, ``isNewBroker`` modifier checks this Broker account doesn't exists in the system.

addInsurer
^^^^^^^^^^

This function adds the new insurer account to the system. ::

    function addInsurer(string memory _socialSecurityId, address _address, string memory _name, string memory _documentsUri) public isNewIsnsurer(_address)

Parameters:
    * ``_socialSecurityId`` - Social Security ID of the Insurer.
    * ``_address`` - Account address of the Insurer.
    * ``_name`` - Insurer name.
    * ``_documentsUri`` - IPFS hash value of the Insurer documents.

This function will create a new Insurer object with role=INSURER and state=PENDING and map it to the account address.
Before this function executes, ``isNewInsurer`` modifier checks this Insurer account doesn't exists in the system.

addBorrower
^^^^^^^^^^^

This function adds the new borrower account to the system. ::

    function addBorrower(string memory _socialSecurityId, address _address, string memory name, string memory _documentsUri) public isBroker(msg.sender) isNewBorrower(_address)

Parameters:
    * ``_socialSecurityId`` - Social Security ID of the Borrower.
    * ``_address`` - Account address of the Borrower.
    * ``_name`` - Insurer name.
    * ``_documentsUri`` - IPFS hash value of the Borrower documents.

This function will create a new Borrower object with role=BORROWER and state=PENDING and map it to the account address.
Before this function executes, ``isNewBorrower`` modifier checks this Borrower account doesn't exists in the system.
``isBroker`` will allow only Brokers to add Borrowers into the system.

approveBorrower
^^^^^^^^^^^^^^^^

This function approves the Borrower request created by Broker. ::

    function approveBorrower(address _address) public isOwner(msg.sender)

Parameters:
    * ``_address`` - Address of the Borrower.

This function sets the Borrower attribute isBankApproved to ``true`` and changes the user state to ``APPROVED``.

approveBroker
^^^^^^^^^^^^^

This function approves the Broker registration request. ::

    function approveBroker(address _address) public isOwner(msg.sender)

Parameters:
    * ``_address`` - Address of the Broker.

This function sets the Broker attribute isBankApproved to ``true`` and changes the user state to ``APPROVED``.

approveInsuranceCompany
^^^^^^^^^^^^^^^^^^^^^^^

This function approves the Broker registration request. ::

    function approveInsuranceCompany(address _address) public isOwner(msg.sender)

Parameters:
    * ``_address`` - Address of the Insurer.

This function sets the Insurer attribute isBankApproved to ``true`` and changes the user state to ``APPROVED``.

verifyIsBroker
^^^^^^^^^^^^^^

This function verifies given account address is a Broker account or not. ::

    function verifyIsBroker(address _address) public view returns(bool)

Parameters:
    * ``_address`` - The account address of the user

This function is used by other smart contracts to verify a Broker account. 
This function will return ``true`` if brokers exists on the given address or ``false`` otherwise.


verifyIsBorrower
^^^^^^^^^^^^^^^^^

This function verifies given account address is a Borrower account or not. ::

    function verifyIsBorrower(address _address) public view returns(bool)

Parameters:
    * ``_address`` - The account address of the user

This function is used by other smart contracts to verify a Borrower account. 
This function will return ``true`` if Borrower exists on the given address or ``false`` otherwise.

verifyIsInsurer
^^^^^^^^^^^^^^^

This function verifies given account address is a Insurer account or not. ::

    function verifyIsInsurer(address _address) public view returns(bool)

Parameters:
    * ``_address`` - The account address of the user

This function is used by other smart contracts to verify a Insurer account. 
This function will return ``true`` if Insurer exists on the given address or ``false`` otherwise.

getAllBrokers
^^^^^^^^^^^^^

This function returns all the Brokers as an array. ::

    function getAllBrokers() public view returns (User[] memory)

getAllInsurers
^^^^^^^^^^^^^^

This functions returns all the Inurers as an array. ::

    function getAllInsurers() public view returns (User[] memory)

getAllBorrowers
^^^^^^^^^^^^^^^

This functions returns all the Borrowers as an array. ::

    function getAllBorrowers() public view returns (Borrower[] memory)

Events
~~~~~~

These events will use in the 

* ``newBrokerAdded`` - This event will emit after successfully create a Broker.
* ``newBorrowerAdded`` - This evevnt will emit after successfully create a Borrower.
* ``newInsurerAdded`` - This event will emit after successfully create a Insurer.

Bank Loan Smart Contract - BankLoan.sol
---------------------------------------

This smart contract stores the Bank Loan details. 
The Bank is the owner of this smart contract.
The following sections describe the components of the smart contract.

ENUM
~~~~


1. LoanState - This enum holds individual loan states. There are 14 loan states. ::

    enum LoanState{
        REQUESTED, 
        INSURANCE_APPLIED, 
        INSURANCE_APPROVED, 
        BORROWER_SIGNED, 
        INSURANCE_REJECTED,
        BANK_APPROVED, 
        BANK_REJECTED, 
        PAID_TO_BROKER, 
        PAID_TO_INSURANCE, 
        ONGOING, 
        DEFAULT, 
        CLOSE,
        CLAIM_REQUESTED, 
        CLAIMED
    }

* ``REQUESTED`` - Initial state of a loan. Broker request a loan. 
* ``INSURANCE_APPLIED`` - Broker applied for a insurance. 
* ``INSURANCE_APPROVED`` - Insurance approved by the Insurance Company.
* ``BORROWER_SIGNED`` -Borrower agreed for the Loan. 
* ``INSURANCE_REJECTED`` - Insurance Company rejected the insurance request for the loan.
* ``BANK_APPROVED`` - Bank approved the Loan
* ``BANK_REJECTED`` - Bank rejected the Loan
* ``PAID_TO_BROKER`` - Bank paid the Broker fee. 
* ``PAID_TO_INSURANCE`` - Bank paid the initial insurance fee. 
* ``ONGOING`` - Bank transfer tokens to the Borrower's account. 
* ``DEFAULT`` - Borrower unable to pay back the Loan. 
* ``CLOSE`` - Borrower paid back the Loan.
* ``CLAIM_REQUESTED`` - Bank requested claim for the defaulted loan.
* ``CLAIMED`` - Insurance Company approved the claim request and transfer tokens to the Bank.


Structs
~~~~~~~

1. Loan - This struct holds the Loan attributes. ::

    struct Loan
    {
        uint id;
        uint amount;
        uint months;
        uint interest;
        uint planId;
        LoanState state;
        address broker;
        address borrower;
        bool bankApprove;
        bool isBorrowerSigned;
        address insurance;
        uint insurancePolicyId;
    }

* ``id`` - Loan Id
* ``amount`` - Loan amount
* ``months`` - Loan duration in months.
* ``interest`` - Loan interest
* ``planId`` - Loan plan Id
* ``state`` - Current state of the loan
* ``broker`` - Address of the Broker who applied the Loan.
* ``borrower`` - Address of the Borrower of the Loan
* ``bankApprove`` - Status of the Bank approval for the Loan
* ``isBorrowerSigned`` - Borrower Signed status.
* ``insurance`` - Insurance Company address.
* ``insurancePolicyId`` - Insurance Policy Id

Events
~~~~~~

These events were defined in the ``BankLoan`` smart contract.

loanRequest
^^^^^^^^^^^

This event will emit when Broker create a loan request. ::

    event loanRequest(
        uint id,
        uint amount,
        uint months, 
        uint interest,
        uint planId, 
        LoanState state, 
        address broker, 
        address borrower,
        bool bankApprove, 
        bool isBorrowerSigned, 
        address insurance, 
        uint insurancePolicyId
    );

Parameters:
    * ``id`` -  Loan Id
    * ``amount`` - Loan amount
    * ``months`` - Duration of the loan
    * ``interest`` - Loan interest
    * ``planId`` - Loan plan id
    * ``state`` - Current state of the loan
    * ``broker`` - Broker of the loan
    * ``borrower`` - Borrower address of the loan
    * ``bankApprove`` - Bank approval status
    * ``isBorrowerSigned`` - Borrower signed status
    * ``insurance`` - Isurer address of the loan
    * ``insurancePolicyId`` - Insurance policy id of the loan


Modifiers
~~~~~~~~~

The following modifiers are used in the ``BankLoan.sol`` functions.

* ``isAdmin()`` - Checks the function callers is the owner of the smart contract.
* ``isBroker()`` - Checks the functiona caller is registered as a Broker in the system.
* ``isLoanBroker(_loanId)`` - Checks the function caller is the Broker of the given Loan.
* ``isLoanBorrower(uint _loanId)`` - Checks the function callers is the Borrower of the given Loan.
* ``isValidLoan(uint _loanId)`` - Checks Loan exists in the system.
* ``isLoanIn(uint _loanId, LoanState _state)`` - Checks the given Loan is in given Loan State.

Attributes
~~~~~~~~~~

* ``UserIdentity: identitySC`` -  Stores UserIdentity smart contract object
* ``address: admin`` - Store smart contract deployer’s address 
* ``Loan[]: loans`` - Stores loan data

Constructor
~~~~~~~~~~~

The constructor will assign the admin address as the contract deployer(``msg.sender``) address. 
It will require the ``UserIdentity`` smart contract address to deploy the smart contract. 
``UserIdentity`` smart contract address object instance will set as the ``identitySC``. ::

    constructor (address _identitySC) {
            admin = msg.sender;
            identitySC = UserIdentity(_identitySC);
    }


Functions
~~~~~~~~~

applyLoan(...)
^^^^^^^^^^^^^^

Creates a Loan request. ::

    function applyLoan(uint _amount, uint _months, uint _interest, uint _planId, address _borrower) public isBroker()

Parameters: 
    * ``_amount`` - Loan amount
    * ``_months`` - Duration of the loan
    * ``_interest`` - Loan interest
    * ``_planId`` -  Loan plan id
    * ``_borrower`` - Borrower address

Modifiers:
    * ``isBroker`` - Checks the function caller registered as a Broker.

addInsurance(...)
^^^^^^^^^^^^^^^^^

Adds Insurance to the given Loan. ::

    function addInsurance(uint _loanId, address _insurance, uint _insurancePolicyId) public isLoanBroker() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.REQUESTED)

Parameters: 
    * ``_loanId`` - Loan Id
    * `` _insurance`` - Insurance address
    * ``_insurancePolicyId`` - Insurance Policy Id

Modifiers:
    * ``isLoanBroker()`` - The function caller should be the Broker of the Loan.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.REQUESTED)`` - Checks Loan is in REQESTED state.

Events:
    * ``loanRequest`` - This function will emits loanRequest event.

insuranceApproved(...)
^^^^^^^^^^^^^^^^^^^^^^

Change the Loan state to Insurance approved state. ::

    function insuranceApproved(uint _loanId) public isLoanBroker() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.INSURANCE_APPLIED)

Parameters: 
    * ``_loanId`` - Loan Id

Modifiers:
    * ``isLoanBroker()`` - The function caller should be the Broker of the Loan.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.INSURANCE_APPLIED)`` - Checks Loan is in INSURANCE_APPLIED state.

insuranceRejected(...)
^^^^^^^^^^^^^^^^^^^^^^

This function updates the Loan state to Insurance Rejected state. ::

    function isuranceRejected(uint _loanId) public isLoanBroker(_loanId) isValidLoan(_loanId) isLoanIn(_loanId, LoanState.INSURANCE_APPLIED)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isLoanBroker()`` - The function caller should be the Broker of the Loan.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.INSURANCE_APPLIED)`` - Checks Loan is in INSURANCE_APPLIED state.

signByBorrower(...)
^^^^^^^^^^^^^^^^^^^

This function is used to sign the Loan by Borrower. ::

    function signByBorrower(uint _loanId) public isLoanBorrower(_loanId) isValidLoan(_loanId) isLoanIn(_loanId, LoanState.INSURANCE_APPROVED)
    
Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isLoanBorrower()`` - The function caller should be the Borrower of the Loan.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.INSURANCE_APPROVED)`` - Checks Loan is in INSURANCE_APPLIED state.

approveLoan(...)
^^^^^^^^^^^^^^^^

This function changes the ``bankApprove`` value to ``True`` and change the Loan state to ``BANK_APPROVED`` state. ::
    
    function approveLoan(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.BORROWER_SIGNED)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.BORROWER_SIGNED)`` - Checks Loan is in BORROWER_SIGNED state.

rejectLoan(...)
^^^^^^^^^^^^^^^

This function changes the ``bankApprove`` value to ``False`` and change the Loan state to ``BANK_REJECTED`` state. ::

    function rejectLoan(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.BORROWER_SIGNED)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.BORROWER_SIGNED)`` - Checks Loan is in BORROWER_SIGNED state.

confirmTokenTrasferToInsurance(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This function changes the Loan state to PAID_TO_INSURANCE. ::

    function confirmTokenTrasferToInsurance(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.BANK_APPROVED)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.BANK_APPROVED)`` - Checks Loan is in BANK_APPROVED state.

confirmTokenTrasferToBroker(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This function changes the Loan state to PAID_TO_BROKER. ::

    function confirmTokenTrasferToBroker(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.PAID_TO_INSURANCE)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.PAID_TO_INSURANCE)`` - Checks Loan is in PAID_TO_INSURANCE state.

confirmTokenTrasferToBorrower(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This function changes the Loan state to ONGOING. ::

    function confirmTokenTrasferToBorrower(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.PAID_TO_BROKER)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.PAID_TO_BROKER)`` - Checks Loan is in PAID_TO_BROKER state.

closeLoan(...)
^^^^^^^^^^^^^^ 

This function changes the Loan state to CLOSE. ::

    function closeLoan(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.ONGOING)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.ONGOING)`` - Checks Loan is in ONGOING state.

markAsDefaulted(...)
^^^^^^^^^^^^^^^^^^^^

This function changes the Loan state to DEFAULT. ::

    function markAsDefaulted(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.ONGOING)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.ONGOING)`` - Checks Loan is in ONGOING state.

requestClaim(...) 
^^^^^^^^^^^^^^^^^

This function changes the Loan state to CLAIM_REQUESTED. ::

    function requestClaim(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.DEFAULT)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.DEFAULT)`` - Checks Loan is in DEFAULT state.

confirmRecivingOfClaim(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^ 

This function changes the Loan state to CLAIMED. ::

    function confirmRecivingOfClaim(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.CLAIM_REQUESTED)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.CLAIM_REQUESTED)`` - Checks Loan is in CLAIM_REQUESTED state.

viewLoan(...)
^^^^^^^^^^^^^

This function returns the Loan. ::

    function viewLoan(uint _loanId) public view
    returns(uint id, uint amount, uint months, uint interest, uint planId, address broker, address borrower, address insurance,
            uint insurancePolicyId, bool bankApprove, bool isBorrowerSigned)

Parameters:
    * ``_loanId`` -  Loan Id


getLoans()
^^^^^^^^^^^^^

This function returns all the Loans. ::

    function getLoans() public view returns(Loan [] memory)


Insurance Policy Smart Contract
-------------------------------

This smart contract stores the Insurance Policy data applied for Loans. 
The Insurance Company is the owner of this smart contract.
The following sections describe the components of the smart contract.

ENUM
~~~~

1. InsuranceState - This enum holds individual Insurance Policy states. There are 11 Insurance Policy states. ::

    enum InsuranceState{
        REQUESTED, 
        BORROWER_SIGNED,
        APPROVED, 
        REJECTED, 
        ONGOING, 
        BORROWER_PAID, 
        CLAIM_REQUESTED,
        CLAIM_APPROVED,
        CLAIM_REJECTED,
        CLAIMED, 
        CLOSE 
    }

* ``REQUESTED`` - Initial state of a Insurance Policy. Broker request a insurance policy foe the Bank Loan. 
* ``BORROWER_SIGNED`` - Borrower agreed for the Insurance Policy and signed the contract.
* ``APPROVED`` - Insurance Company approved the Insurance Policy request.
* ``REJECTED`` - Insurance Company rejected the Insurance Policy request.
* ``ONGOING`` - Bank paid the initial payment. Loan started. Insurance policy covering the Loan. 
* ``BORROWER_PAID`` - Borrower paid the final payment for the covering the Loan. 
* ``CLAIM_REQUESTED`` - Bank request a claim for the defaulted Loan.
* ``CLAIM_APPROVED`` - Insurance Company approved the claim request made by the Bank.
* ``CLAIM_REJECTED`` - Insurance Comapny rejected the claim request.
* ``CLAIMED`` - Insurance Company paid the claim to the Bank. 
* ``CLOSE`` - Close the Insurance Policy. No longer covering the Bank Loan.


Structs
~~~~~~~

1. Policy - This Policy struct holds the Insurance Policy attributes. ::
    
    struct Policy
    {
        uint id;
        uint amount;
        uint months;
        uint cover;
        uint planId;
        uint loanId;
        InsuranceState state;
        address broker;
        address borrower;
        bool insuranceApprove;
        bool isBorrowerSigned;
    }

* ``id`` - Insurance policy Id.
* ``amount`` - Insurance policy fee.
* ``months`` - Insurance validity period.
* ``cover`` - Loan amount covered by the Insurance.
* ``planId`` - Insurance plan id.
* ``loanId`` - Bank Loan Id.
* ``state`` - Current state of the Insurance Policy 
* ``broker`` - Broker of the Bank Loan/ Insurance Policy
* ``borrower`` - Borrower of the Bank Loan.
* ``insuranceApprove`` - Insurance Company approval status for the Insurance Policy.
* ``isBorrowerSigned`` - Borrower signed status.

Events
~~~~~~

Events defined in the ``InsurancePolicy.sol`` smart contract.

insurancePolicyRequest
^^^^^^^^^^^^^^^^^^^^^^

This event will emit when Broker create a Insurance Policy request. ::

    event insurancePolicyRequest(
        uint id,
        uint amount,
        uint months,
        uint cover,
        uint planId,
        uint loanId,
        InsuranceState state,
        address broker,
        address borrower,
        bool insuranceApprove,
        bool isBorrowerSigned
    );

Parameters: 
    * ``id`` - Insurance policy Id.
    * ``amount`` - Insurance policy fee.
    * ``months`` - Insurance validity period.
    * ``cover`` - Loan amount covered by the Insurance.
    * ``planId`` - Insurance plan id.
    * ``loanId`` - Bank Loan Id.
    * ``state`` - Current state of the Insurance Policy 
    * ``broker`` - Broker of the Bank Loan/ Insurance Policy
    * ``borrower`` - Borrower of the Bank Loan.
    * ``insuranceApprove`` - Insurance Company approval status for the Insurance Policy.
    * ``isBorrowerSigned`` - Borrower signed status.

Modifiers
~~~~~~~~~

The following modifiers are used in the ``InsurancePolicy.sol`` functions.

1. ``isAdmin()`` - Checks the function callers is the owner of the smart contract.
2. ``isBroker()`` - Checks the functiona caller is registered as a Broker in the system.
3. ``isValidInsurance(uint _insuranceId)`` - Checks Insurance Policy exists in the system.

Attributes
~~~~~~~~~~

1. ``UserIdentity: identitySC`` - Stores UserIdentity smart contract object
2. ``address private admin`` - Store smart contract deployer’s address
3. ``Policy[] private policies`` - Stores insurance policy data

Constructor
~~~~~~~~~~~

``InsurancePolicy.sol`` takes ``UserIdentity.sol`` address as a parameter for the constructor function.
It sets the deployer's address as the smart contract ``admin`` address. ::

    constructor (address _identitySC) {
            admin = msg.sender;
            identitySC = UserIdentity(_identitySC);
    }

Functions
~~~~~~~~~

applyInsurance(...)
^^^^^^^^^^^^^^^^^^^

This function creates a Insurance Policy request. ::

    function applyInsurance(uint amount, uint months, uint cover, uint planId, uint loanId, address borrower) public isAdmin()

signContract(...)
^^^^^^^^^^^^^^^^^

This function sets the Insurance Policy attribute ``isBorrowerSigned`` to ``True`` and 
changes the Insurance Policy state to BORROWER_SIGNED. ::

    function signContract(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    

approveInsurancePolicy(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^
    
This function changes the Insurance Policy attribute ``insuranceApprove`` value to ``True`` and 
changes the Insurance Policy state to APPROVED. ::

    function approveInsurancePolicy(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    
rejectInsurancePolicy(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^

This function changes the Insurance Policy attribute ``insuranceApprove`` value to ``False`` and 
changes the Insurance Policy state to REJECTED. ::

    function rejectInsurancePolicy(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)

confirmPaymentFromBank(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to ONGOING. ::

    function confirmPaymentFromBank(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)


confirmPaymentFromBorrower(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  

This function changes the Insurance Policy state to BORROWER_PAID. ::

    function confirmPaymentFromBorrower(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)
    
closePolicy(...)
^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to CLOSE. ::

    function closePolicy(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)

requestClaim(...)
^^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to CLAIM_REQUESTED. ::
    
    function requestClaim(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)

rejectClaim(...)
^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to CLAIM_REJECTED. ::
    
    function rejectClaim(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)

approveClaim(...)
^^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to CLAIM_APPROVED. ::
    
    function approveClaim(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)

confirmClaim(...)
^^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to CLAIMED. ::

    
    function confirmClaim(uint _insuranceId) public isAdmin() isValidInsurance(_insuranceId)


viewInsurancePolicy(...)
^^^^^^^^^^^^^^^^^^^^^^^^

This function returns the requested Insurance Policy details. ::
    
    function viewInsurancePolicy(uint _insuranceId) public view isAdmin() isValidInsurance(_insuranceId) 
    returns(uint id,
        uint amount,
        uint months,
        uint cover,
        uint planId,
        uint loanId,
        InsuranceState state,
        address broker,
        address borrower,
        bool insuranceApprove,
        bool isBorrowerSigned)
    
getAllPolicies(...)
^^^^^^^^^^^^^^^^^^^

This function returns all the Loans. ::

    function getAllPolicies() public view returns(Policy [] memory)


Loan Payment Smart Contract
---------------------------

We assume that non-banking Borrowers will repay their loans with fiat cash. 
This cash will collect by Brokers. 
Brokers will buy tokens from Bank and pay Borrowers' loans in tokens.
To record these money hand over events we use ``LoanPayment.sol`` smart contract.

ENUM
~~~~

PaymentState
^^^^^^^^^^^^

This enum holds individual money handover event states. Initially there are 6 states. ::

    enum PaymentState { 
        HANDSHAKE_START, 
        HANDSHAKE_SUCCESS, 
        MONEY_HANDEDOVER, 
        MONEY_RECEIVED, 
        FLAG_TRANSACTION, 
        CANCEL 
    }

* ``HANDSHAKE_START`` - Initial state of a Payment. Borrower handshakes with Broker to handover money.
* ``HANDSHAKE_SUCCESS`` - Broker accepts the handshake and willing to collect fiat cash from Borrower.
* ``MONEY_HANDEDOVER`` - Borrower handover money to Broker.
* ``MONEY_RECEIVED`` - Broker confirms that he receives correct amunt of money.
* ``FLAG_TRANSACTION`` - Broker or Borrower did a fraud. Other party flag the money handover process.
* ``CANCEL`` - Broker or Borrower cancelled the money hand over process.

Structs
~~~~~~~

Payment
^^^^^^^

This struct holds payment attributes. ::

    struct Payment{
        uint id;
        uint pin;
        address borrower;
        address broker;
        uint loanId;
        uint tokenAmount;
        uint amount;
        PaymentState status;
    }

Parameters:
    * ``id`` - Payment id
    * ``pin`` - 4 digit pin defined by Borrower
    * ``borrower`` - address of the Borrower
    * ``broker`` - address of the Broker
    * ``loanId`` - Bank loan Id.
    * ``tokenAmount`` - Payment value in token
    * ``amount`` - Payment value in fiat cash
    * ``status`` - State of the Payment

Attributes
~~~~~~~~~~

* ``admin :address`` - Stores smart contract deployer’s address ad admin address.
* ``identitySC :UserIdentity`` -  Stores UserIdentity smart contract object.
* ``payments :Payment[]`` - Stores payments data.

Constructor
~~~~~~~~~~~

``LoanPayment.sol`` takes ``UserIdentity.sol`` address as a parameter for the constructor function. 
It sets the deployer’s address as the smart contract's admin address. ::

    constructor (address _identitySC) {
        admin = msg.sender;
        identitySC = UserIdentity(_identitySC);
    }

Events
~~~~~~

There are 6 events emits when money handover state change.

paymentHandshake
^^^^^^^^^^^^^^^^

This event will emit when Borrower wishes to handover money and starts a handshake with Broker. ::

    event paymentHandshake(
        uint id,
        uint pin,
        address borrower,
        address broker,
        uint loanId,
        uint tokenAmount,
        uint amount,
        PaymentState status
    );

Parameters:
    * ``id`` - Payment id
    * ``pin`` - 4 digit pin defined by Borrower
    * ``borrower`` - address of the Borrower
    * ``broker`` - address of the Broker
    * ``loanId`` - Bank loan Id.
    * ``tokenAmount`` - Payment value in token
    * ``amount`` - Payment value in fiat cash
    * ``status`` - State of the Payment

paymentHandshakeConfirm
^^^^^^^^^^^^^^^^^^^^^^^

When Broker accepts the handshake request this event will emit. ::
    
    event paymentHandshakeConfirm(
        uint id,
        PaymentState status
    );

Parameters:
    * ``id`` - Payment id
    * ``status`` - State of the Payment

moneyHandover
^^^^^^^^^^^^^

When Broker accepts the handshake Borrower will handover the money to Broker.
Then Borrower can uodate the payment status. 
When Borrower records the money handover then this event will emit. ::

    event moneyHandover(
        uint id,
        PaymentState status
    );

Parameters:
    * ``id`` - Payment id
    * ``status`` - State of the Payment

moneyReceived
^^^^^^^^^^^^^

When Borrwer hand over the money and update it in the system Broker should confirm that he received the correct amount of money.
When Broker confirms he received the correct amount of money in the system this event will emit. ::
    
    event moneyReceived(
        uint id,
        PaymentState status
    );

Parameters:
    * ``id`` - Payment id
    * ``status`` - State of the Payment
    
paymentCancel
^^^^^^^^^^^^^

Broker or Borrower can cancel the money handover process before handover money to Broker.
When someone cancels the process this event will emit with the reason to cancel the process. ::

    event paymentCancel(
        uint id,
        PaymentState status,
        string reason
    );

Parameters:
    * ``id`` - Payment id
    * ``status`` - State of the Payment
    * ``reason`` - reason to cancel the money handover process.

flagPayment
^^^^^^^^^^^

If Broker or Borrower does any fraud during this process other party(Borrower or Broker) can flag this process. 
It will motify by all stakeholders.
when Broker or Borrower flag the process this event will emit. ::
    
    event flagPayment(
        uint id,
        PaymentState status,
        string reason
    );

Parameters:
    * ``id`` - Payment id
    * ``status`` - State of the payment
    * ``reason`` - reason to flag the payment.

Modifiers
~~~~~~~~~

isBroker
^^^^^^^^

Checks the ``_address`` is registered as Broker in the system. ::

    modifier isBroker(address _broker){
        require(identitySC.verifyIsBroker(_broker), 'Broker Only');
        _;
    }
    
isPaymentIn
^^^^^^^^^^^

Checks the payment is in ``_state``. ::

    modifier isPaymentIn(uint _paymentId, PaymentState _state){
        bool isValid = false;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId && payments[i].status == _state)
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }

checkPin
^^^^^^^^

Checks given ``_pin`` is correct for the given Payment. ::
    
    modifier checkPin(uint _paymentId, uint _pin){
        bool isValid = false;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId && payments[i].pin == _pin)
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }

checkBroker
^^^^^^^^^^^

This modifier checks whether the input ``_broker`` address and the associated 
Broker address of the payment that binds to the parameter  ``_paymentId`` is similar or not. ::
    
    modifier checkBroker(uint _paymentId, address _broker){
        bool isValid = false;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId && payments[i].broker == _broker)
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }

Parameters:
    * ``_paymentId`` - Payment Id
    * ``_broker`` - Broker address

checkBorrower
^^^^^^^^^^^^^

This modifier checks whether the input ``_borrower`` address and the associated 
Borrower address of the payment that binds to the parameter  ``_paymentId`` is similar or not. ::
    
    modifier checkBorrower(uint _paymentId, address _borrower){
        bool isValid = false;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId && payments[i].borrower == _borrower)
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }

Parameters:
    * ``_paymentId`` - Payment Id
    * ``_borrower`` - Borrower address


Functions
~~~~~~~~~

startHandshake
^^^^^^^^^^^^^^

This function creates a new Payment and set its state to ``HANDSHAKE_START``. 
Borrower can start a new Payment.
This functions emits ``paymentHandshake`` event. ::

    function startHandshake(uint _loanId, uint _amount, uint _tokens, address _broker, uint _pin) public isBroker(_broker)

Parameters:
    * ``_loanId`` - Loan Id
    * ``_amount`` -  Amount of fiat cash Borrower willing to hand over.
    * ``_tokens`` - Payment amount in tokens.
    * ``_broker`` - Broker address who is recieving the money. 
    * `` _pin`` - Arbitary Pin number decided by Borrower.

Modifiers:
    * ``isBroker(_broker) - Checks ``_broker`` address registered as Broker in the ``UserIdentity.sol``.

Events:
    * ``paymentHandshake`` - This function emits ``paymentHandshake`` event when successfully creates payment.
    
confirmHandshake
^^^^^^^^^^^^^^^^

This function changes the payment state to ``HANDSHAKE_SUCCESS``. ::

    function confirmHandshake(uint _paymentId, uint _pin) 
        public 
        isBroker(msg.sender) 
        isPaymentIn(_paymentId, PaymentState.HANDSHAKE_START) 
        checkPin(_paymentId, _pin) 
        checkBroker(_paymentId, msg.sender)

Parameters:
    * ``_loanId`` - Loan Id
    * ``_amount`` -  Amount of fiat cash Borrower willing to hand over.
    * ``_tokens`` - Payment Amount in token.
    * ``_broker`` - Broker address who is recieving the money.
    * `` _pin`` - Pin number for the payment.

Modifiers:
    * ``isBroker(msg.sender)`` - Checks function caller registered as Broker in the ``UserIdentity.sol``.
    * ``isPaymentIn(_paymentId, PaymentState.HANDSHAKE_START)`` -  Checks payment is in  ``HANDSHAKE_START`` state.
    * ``checkPin(_paymentId, _pin)`` - Checks parameter ``_pin`` correct respect to the payment binds to the ``_paymentId``.
    * ``checkBroker(_paymentId, msg.sender)`` - Check function caller is Broker registered to the paymemnt binds to the ``_paymentId``.


Events:
    * ``paymentHandshakeConfirm`` - This function emits ``paymentHandshakeConfirm`` event when it changes the payment state to ``HANDSHAKE_SUCCESS``.
    
handoverMoney
^^^^^^^^^^^^^

This function changes a payment state to ``MONEY_HANDEDOVER``. If successfull, It emits ``moneyHandover`` event . ::

    function handoverMoney(uint _paymentId) 
        public 
        checkBorrower(_paymentId, msg.sender) 
        isPaymentIn(_paymentId, PaymentState.HANDSHAKE_SUCCESS)

Parameters:
    * ``_paymentId`` - Payment Id

Modifiers:
    * ``checkBorrower(msg.sender)`` - Checks function caller registered as the Borrower for the payment.
    * ``isPaymentIn(_paymentId, PaymentState.HANDSHAKE_SUCCESS)`` -  Checks payment is in  ``HANDSHAKE_SUCCESS`` state.

Events:
    * ``moneyHandover`` - This function emits ``moneyHandover`` event when it successfully changes the payment state to ``MONEY_HANDEDOVER``.
 
confirmMoneyReceived
^^^^^^^^^^^^^^^^^^^^

This function changes payment state to MONEY_HANDEDOVER and emits moneyReceived event.
    
    function confirmMoneyReceived(uint _paymentId) 
        public 
        checkBroker(_paymentId, msg.sender) 
        isPaymentIn(_paymentId, PaymentState.MONEY_HANDEDOVER)
    {
        uint x = 0;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId)
            {
                payments[i].status = PaymentState.MONEY_RECEIVED;
                x = i;
                break;
            }
        }
        
        emit moneyReceived(payments[x].id, payments[x].status);
    }

Parameters:
    * ``_paymentId`` - Payment Id

Modifiers:
    * ``checkBroker(_paymentId, msg.sender)`` - Checks function caller registered as the Broker for the payment binds to the ``_paymentId``.
    * ``isPaymentIn(_paymentId, PaymentState.MONEY_HANDEDOVER)`` - Checks payment is in MONEY_HANDEDOVER state.

Events:
    * ``moneyReceived`` - This function emits ``moneyReceived`` event when it successfully changes the payment state to ``MONEY_RECEIVED``.

cancelByBorrower
^^^^^^^^^^^^^^^^

This function changes payment state to *CANCEL* and emits *paymentCancel* event.
Function caller should be registered as the Borrower for the Payment. 
Payment should be in *HANDSHAKE_SUCCESS* state. ::

    function cancelbyBorrower(uint _paymentId) 
        public 
        checkBorrower(_paymentId, msg.sender) 
        isPaymentIn(_paymentId, PaymentState.HANDSHAKE_SUCCESS)
    
Parameters:
    * ``_paymentId`` - Payment Id

Modifiers:
    * ``checkBorrower(msg.sender)`` - Checks function caller registered as the Borrower for the payment.
    * ``isPaymentIn(_paymentId, PaymentState.HANDSHAKE_SUCCESS)`` -  Checks payment is in  ``HANDSHAKE_SUCCESS`` state.

Events:
    * ``paymentCancel`` - This function emits *paymentCancel* event when it successfully changes the payment state to *CANCEL*.

cancelByBroker
^^^^^^^^^^^^^^

This function changes payment state to *CANCEL* and emits *paymentCancel* event.
Function caller should be registered as the Broker for the Payment.
Payment state should be in *HANDSHAKE_START*. ::
    
    function cancelbyBroker(uint _paymentId) 
        public 
        checkBroker(_paymentId, msg.sender) 
        isPaymentIn(_paymentId, PaymentState.HANDSHAKE_START)

Parameters:
    * ``_paymentId`` - Payment Id

Modifiers:
    * ``checkBroker(msg.sender)`` - Checks function caller registered as the Broker for the payment.
    * ``isPaymentIn(_paymentId, PaymentState.HANDSHAKE_START)`` -  Checks payment is in  ``HANDSHAKE_START`` state.

Events:
    * ``paymentCancel`` - This function emits *paymentCancel* event when it successfully changes the payment state to *CANCEL*.
    
flagByBorrower
^^^^^^^^^^^^^^

This function changes payment state to *FLAG_TRANSACTION* and emits *paymentCancel* event.
Function caller should be registered as the Borrower for the Payment.
Payment should be in *MONEY_HANDEDOVER* state. ::

    function flagByBorrower(uint _paymentId) 
        public 
        checkBorrower(_paymentId, msg.sender) 
        isPaymentIn(_paymentId, PaymentState.MONEY_HANDEDOVER)
    {
        uint x = 0;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId)
            {
                payments[i].status = PaymentState.FLAG_TRANSACTION;
                x = i;
                break;
            }
        }
        
        emit flagPayment(payments[x].id, payments[x].status, "Payment was flagged by Borrower");
    }

Parameters:
    * ``_paymentId`` - Payment Id.

Modifiers:
    * ``checkBorrower(msg.sender)`` - Checks function caller registered as the Borrower for the payment.
    * ``isPaymentIn(_paymentId, PaymentState.MONEY_HANDEDOVER)`` -  Checks payment is in  *MONEY_HANDEDOVER* state.

Events:
    * ``flagPayment`` - This function emits *flagPayment* event when it successfully changes the payment state to *FLAG_TRANSACTION*.

flagByBroker
^^^^^^^^^^^^

This function changes payment state to *FLAG_TRANSACTION* and emits *paymentCancel* event.
Function caller should be registered as the Broker for the Payment.
Payment should be in *MONEY_HANDEDOVER* state. ::
    
    function flagByBroker(uint _paymentId) public 
        checkBroker(_paymentId, msg.sender) isPaymentIn(_paymentId, PaymentState.MONEY_HANDEDOVER)
    {
        uint x = 0;
        for(uint i=0; i< payments.length; i++)
        {
            if(payments[i].id == _paymentId)
            {
                payments[i].status = PaymentState.FLAG_TRANSACTION;
                x = i;
                break;
            }
        }
        
        emit flagPayment(payments[x].id, payments[x].status, "Payment was flagged by Broker");
    }

Parameters:
    * ``_paymentId`` - Payment Id.

Modifiers:
    * ``checkBroker(msg.sender)`` - Checks function caller registered as the Broker for the payment.
    * ``isPaymentIn(_paymentId, PaymentState.MONEY_HANDEDOVER)`` -  Checks payment is in  *MONEY_HANDEDOVER* state.

Events:
    * ``flagPayment`` - This function emits *flagPayment* event when it successfully changes the payment state to *FLAG_TRANSACTION*.

getAllPayments
^^^^^^^^^^^^^^

This function returns all Payment data stored in the *LoanPayment* smart contract. 
This function returns array of *Payment*. ::

    function getAllPayments() public view returns (Payment[] memory)