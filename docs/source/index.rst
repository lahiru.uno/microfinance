.. Microfinance documentation master file, created by
   sphinx-quickstart on Wed Aug 25 19:56:33 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Microfinance's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Introduction:

   introduction/applicationOverview

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Installation Guide:

   installation/guide
   installation/checkout
   installation/structure
   installation/deploySmartContract
   installation/metamask
   installation/bankServer
   installation/bankApp
   installation/insuranceServer
   installation/insuranceApp
   installation/connectReactMetaMask

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Deploy to Ropsten:

   deployToRopsten/guide

.. toctree::
   :maxdepth: 3
   :hidden:
   :caption: Smart Contracts:

   blockchain/introduction
   blockchain/smartContracts
   blockchain/migration
   blockchain/truffleConfig

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Bank Server:

   bankServer/introduction

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Bank Web Application:

   bankClient/introduction
   bankClient/authContext
   bankClient/readFromSmartContract
   

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Insurance Co. Server:

   insuranceServer/introduction

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Insurance Co. Web Application:

   insuranceClient/introduction