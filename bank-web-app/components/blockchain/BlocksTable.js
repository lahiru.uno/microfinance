import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, message, Button, Modal } from 'antd';
import Moment from 'react-moment';
import { getApi } from '../../util/fetchApi';
import AuthContext from '../../stores/authContext';

function BlocksTable() {
	const { web3, MicroTokenAddress, UserIdentityAddress, BankLoanAddress, LoanPaymentAddress } = useContext(AuthContext);

	const columns = [
		{
			title: 'Block ID',
			dataIndex: 'id',
			key: 'id',
			sortOrder: 'descend',
			sorter: (a, b) => a.id - b.id,
			width: '10%',
			// render: text => text,
		},
		{
			title: 'Hash',
			dataIndex: 'hash',
			key: 'hash',
			width: '50%',
			ellipsis: true,
		},
		{
			title: 'Time',
			dataIndex: 'time',
			key: 'time',
			width: '20%',
			render: time => (
				<Moment unix>{time}</Moment>
			),
		},
		{
			title: 'Number of Transactions',
			dataIndex: 'transactionCount',
			key: 'transactionCount',
			width: '20%',
			render: count => (
				<>
					{count === 0 && <Tag color="red"> No Transactions </Tag>}
					{count === 1 && <Tag color="blue"> {count} Transaction </Tag>}
					{count > 1 && <Tag color="blue"> {count} Transactions </Tag>}
				</>
			),
		},
	];

	const [data, setData] = useState([]);

	const addData = async (error, block) => {
		const transactionData = [];

		if (block.transactions.length > 0) {
			for (let i = 0; i < block.transactions.length; i++) {
				const response = await web3.eth.getTransaction(block.transactions[i])
				console.log(response);
				const reciept = await web3.eth.getTransactionReceipt(block.transactions[i])
				console.log(reciept);
				const t = {
					hash: response.hash,
					from: response.from,
					to: response.to,
					status: reciept.status,
					gasUsed: reciept.gasUsed,
					contractAddress: reciept.contractAddress,
					contractName: getContractName(response.to),
					value: response.value,
					gasPrice: response.gasPrice,
					gas: response.gas,
					nonce: response.nonce,
					cumulativeGasUsed: reciept.cumulativeGasUsed,
					input: response.input,
				};
				transactionData.push(t);
			}
		}

		const row = {
			key: block.number,
			id: block.number,
			hash: block.hash,
			time: block.timestamp,
			transactions: block.transactions,
			transactionCount: block.transactions.length,
			transactionData,
		};

		setData((prev) => {
			return [...prev, row];
		});

		console.log(data);
	};

	const getBlocks = async () => {
		try {
			setData([]);

			const latest = await web3.eth.getBlockNumber();

			const batch = new web3.eth.BatchRequest();

			const limit = latest > 20 ? 20 : latest;

			for (let i = 0; i <= limit; i++) {
				batch.add(web3.eth.getBlock.request(latest - i, addData));
			}

			batch.execute();
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading blocks');
		}
	};

	const getContractName = (address) => {
		let contractName;
		if (address === BankLoanAddress) {
			contractName = 'BANK LOAN CONTRACT';
		} else if (address === MicroTokenAddress) {
			contractName = 'MICRO TOKEN CONTRACT';
		} else if (address === LoanPaymentAddress) {
			contractName = 'LOAN PAYMNET CONTRACT';
		} else if (address === UserIdentityAddress) {
			contractName = 'USER IDENTITY CONTRACT';
		}

		return contractName;
	};

	const getContractNameTag = (address, contractName) => {
		if (contractName) {
			return <> {address} <Tag color="geekblue">{contractName}</Tag> </>;
		}
		return <> {address} </>;
	};

	const getTransactionType = (transaction) => {
		if (transaction.transactionData.to && transaction.transactionData.contractName) {
			return <Tag color="orange"> CONTRACT CALL </Tag>;
		}
		if (transaction.transactionData.contractAddress) {
			return <Tag color="tomato"> CONTRACT CREATION </Tag>;
		}
		return <></>;
	};

	const transactionDataModal = (t) => {
		Modal.info({
			title: 'Decoded Transaction Data',
			content: (
				<div>
					<p>Method: {t.method}</p>
					<p>Input Names: [{t.names.map(item => (<span> {item}, </span>))}]</p>
					<p>Input Values: [{t.inputs.map(item => (<span> {item}, </span>))}]</p>
					<p>Input Types: [{t.types.map(item => (<span> {item}, </span>))}]</p>
				</div>
			),
			width: 700,
			onOk() {},
		});
	};

	const decodeTransactionData = async (transactionData) => {
		try {
			if (transactionData.contractName === 'BANK LOAN CONTRACT') {
				const response = await getApi({
					url: 'decode-transaction/bank-loan/' + transactionData.input,
				});
				const t = await response;
				transactionDataModal(t);
			} else if (transactionData.contractName === 'USER IDENTITY CONTRACT') {
				const response = await getApi({
					url: 'decode-transaction/user-identity/' + transactionData.input,
				});
				const t = await response;
				transactionDataModal(t);
			} else if (transactionData.contractName === 'MICRO TOKEN CONTRACT') {
				const response = await getApi({
					url: 'decode-transaction/micro-token/' + transactionData.input,
				});
				const t = await response;
				transactionDataModal(t);
			} else if (transactionData.contractName === 'LOAN PAYMNET CONTRACT') {
				const response = await getApi({
					url: 'decode-transaction/loan-payment/' + transactionData.input,
				});
				const t = await response;
				transactionDataModal(t);
			}
		} catch (err) {
			console.log(err);
			message.error('Error occured while decoding transaction data');
		}
	};

	useEffect(() => {
		getBlocks();
	}, []);

	function expandedTransactionsRow(record) {
		const transactionInfoColumns = [
			{ title: 'Attribute', dataIndex: 'attribute', key: 'attribute', width: '20%' },
			{ title: 'Value', dataIndex: 'value', key: 'value', width: '80%', ellipsis: true },
		];

		const transactionData = [
			{
				attribute: 'From',
				value: record.transactionData.from,
			},
			{
				attribute: 'To',
				// value: getContractName(record.transactionData.to),
				value: getContractNameTag(record.transactionData.to, record.transactionData.contractName),
			},
			{
				attribute: 'Status',
				value: record.transactionData.status ?
					<Tag color="green"> TRUE </Tag> :
					<Tag color="red"> FALSE </Tag>,
			},
			{
				attribute: 'Input',
				value:
					<>
						<p>{record.transactionData.input}</p>
						<Button onClick={() => decodeTransactionData(record.transactionData)}> Decode Transaction Data</Button>
					</>
			},
			{
				attribute: 'Contract address',
				value: record.transactionData.contractAddress,
			},
			{
				attribute: 'Value',
				value: record.transactionData.value,
			},
			{
				attribute: 'Gas',
				value: record.transactionData.gas,
			},
			{
				attribute: 'Gas price',
				value: record.transactionData.gasPrice,
			},
			{
				attribute: 'Gas used',
				value: record.transactionData.gasUsed,
			},
			{
				attribute: 'Cumulative gas used',
				value: record.transactionData.cumulativeGasUsed,
			},
			{
				attribute: 'Nonce',
				value: record.transactionData.nonce,
			},
		];

		return <Table columns={transactionInfoColumns} dataSource={transactionData} pagination={false} size="small" />;
	}

	const expandedRowRender = (record) => {
		console.log(record);

		const expandedData = [];

		for (let i = 0; i < record.transactions.length; i++) {
			const t = {
				id: i + 1,
				transaction: record.transactions[i],
				transactionData: record.transactionData[i],
			};

			expandedData.push(t);
		}

		const expandedColumns = [
			{
				title: 'Transactions',
				dataIndex: 'transaction',
				key: 'transaction',
			},
			{
				title: 'Type',
				dataIndex: '',
				key: '',
				render: transaction => (
					<>
						{getTransactionType(transaction)}
					</>
				),
			},
		];

		return (
			<Table
				columns={expandedColumns}
				dataSource={expandedData}
				pagination={false}
				expandable={{ expandedRowRender: expandedTransactionsRow }}
			/>);
	};

	return (
		<>
			<Card title="Blockchain" extra={<a href="javascript:void(0);" onClick={() => getBlocks()}>Refresh</a>}>
				<Table
					columns={columns}
					dataSource={data}
					expandable={{
						expandedRowRender,
						rowExpandable: record => record.transactions.length > 0,
					}}
				/>
			</Card>
		</>
	);
}

export default BlocksTable;
