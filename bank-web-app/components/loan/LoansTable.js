import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, Divider, message, Modal, Form, InputNumber, Input } from 'antd';
import AuthContext from '../../stores/authContext';
import { getApi } from '../../util/fetchApi';

function LoansTable() {
	const { user, BankLoanContract, UserIdentityContract } = useContext(AuthContext);

	const state = ['REQUESTED', 'INSURANCE_APPLIED', 'INSURANCE_APPROVED', 'BORROWER_SIGNED',
		'INSURANCE_REJECTED', 'BANK_APPROVED', 'BANK_REJECTED', 'PAID_TO_INSURANCE', 'PAID_TO_BROKER',
		'ONGOING', 'DEFAULT', 'CLOSE', 'CLAIM_REQUESTED', 'CLAIMED'];

	const [isModalVisible, setIsModalVisible] = useState(false);
	const [isRejectModalVisible, setIsRejectModalVisible] = useState(false);
	const [isInsuranceModalVisible, setIsInsuranceModalVisible] = useState(false);
	const [id, setId] = useState(-1);

	const [payments, setPayments] = useState([]);

	const [componentSize, setComponentSize] = useState('default');
	const onFormLayoutChange = ({ size }) => {
		setComponentSize(size);
	};

	const [insuranceId, setInsuranceId] = useState('');
	const [insurance, setInsurance] = useState('');

	const [data, setData] = useState([]);

	const brokers = {};
	const borrowers = {};

	const getPayments = async () => {
		try {
			const response = await getApi({
				url: 'loan-payments',
			});
			const paymentsResult = await response;
			setPayments(paymentsResult);
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading Loan Payments');
		}
	};

	const getBrokers = async () => {
		const response = await UserIdentityContract.methods.getAllBrokers().call();
		for (let i = 0; i < response.length; i++) {
			brokers[response[i].userAddress] = response[i].name;
		}
	};

	const getBorrowers = async () => {
		const response = await UserIdentityContract.methods.getAllBorrowers().call();
		for (let i = 0; i < response.length; i++) {
			borrowers[response[i].userAddress] = response[i].name;
		}
	};

	const getLoans = async () => {
		try {
			// const accounts = await window.ethereum.enable();
			// console.log(accounts[0]);
			const response = await BankLoanContract.methods.getLoans().call();

			setData([]);

			for (let i = 0; i < response.length; i++) {
				const row = {
					key: response[i].id,
					id: response[i].id,
					amount: response[i].amount,
					period: response[i].months,
					interest: response[i].interest,
					planId: response[i].planId,
					borrowerName: borrowers[response[i].borrower],
					borrower: response[i].borrower,
					brokerName: brokers[response[i].broker],
					broker: response[i].broker,
					insurance: response[i].insurance,
					InsurancePolicyId: response[i].InsurancePolicyId,
					status: response[i].state,
				};

				// console.log(row);

				setData((prev) => {
					return [...prev, row];
				});
			}
			// console.log(response);
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading current Loans');
		}
	};

	const approveLoan = async () => {
		try {
			const accounts = await window.ethereum.enable();
			// console.log(accounts[0]);
			await BankLoanContract.methods.approveLoan(id).send({ from: accounts[0] });
			message.success(`Loan ${id} approved`);
			getLoans();
		} catch (err) {
			message.error('Error occured while approving the Loan');
		}
	};

	const rejectLoan = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.rejectLoan(id).send({ from: accounts[0] });
			message.success(`Loan ${id} rejected`);
			getLoans();
		} catch (err) {
			message.error('Error occured while rejecting the Loan');
		}
	};

	const addInsuranceToLoan = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.addInsurance(id, insurance, insuranceId).send({ from: accounts[0] });
			message.success(`Insurance added successfully to the Loan ${id}`);
			getLoans();
		} catch (err) {
			message.error('Error occured while adding insurance');
		}
	};

	const notifyInsuranceApproval = async (loanId) => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.insuranceApproved(loanId).send({ from: accounts[0] });
			message.success(`Insurance Approval recorded for Loan ${id}`);
			getLoans();
		} catch (err) {
			message.error('Error occured while updating Loan');
		}
	};

	const notifyInsuranceRejection = async (loanId) => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.isuranceRejected(loanId).send({ from: accounts[0] });
			message.success(`Insurance Rejection recorded for Loan ${id}`);
			getLoans();
		} catch (err) {
			message.error('Error occured while updating Loan');
		}
	};

	// const getLoans = async () => {
	// 	try {
	// 		const response = await getApi({
	// 			url: 'loan',
	// 		});

	// 		const result = await response;
	// 		setData(result);
	// 		await console.log(result);
	// 	} catch (err) {
	// 		console.log(err);
	// 		message.error('Error occured while loading current loans');
	// 	}
	// };

	const signLoan = async (loanId) => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.signByBorrower(loanId).send({ from: accounts[0] });
			message.success(`Loan ${id} signed`);
			getLoans();
		} catch (err) {
			console.log(err);
			message.error('Error occured while signing Loan');
		}
	};

	const confirmTokenTrasferToInsurance = async (loanId) => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.confirmTokenTrasferToInsurance(loanId).send({ from: accounts[0] });
			message.success(`Loan ${id} updated`);
			getLoans();
		} catch (err) {
			console.log(err);
			message.error('Error occured while updating Loan');
		}
	};

	const confirmTokenTrasferToBroker = async (loanId) => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.confirmTokenTrasferToBroker(loanId).send({ from: accounts[0] });
			message.success(`Loan ${id} updated`);
			getLoans();
		} catch (err) {
			console.log(err);
			message.error('Error occured while updating Loan');
		}
	};

	const confirmTokenTrasferToBorrower = async (loanId) => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.confirmTokenTrasferToBorrower(loanId).send({ from: accounts[0] });
			message.success(`Loan ${id} updated`);
			getLoans();
		} catch (err) {
			console.log(err);
			message.error('Error occured while updating Loan');
		}
	};

	const closeLoan = async (loanId) => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.closeLoan(loanId).send({ from: accounts[0] });
			message.success(`Loan ${id} updated`);
			getLoans();
		} catch (err) {
			console.log(err);
			message.error('Error occured while updating Loan');
		}
	};

	const markAsDefaulted = async (loanId) => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.markAsDefaulted(loanId).send({ from: accounts[0] });
			message.success(`Loan ${id} updated`);
			getLoans();
		} catch (err) {
			console.log(err);
			message.error('Error occured while updating Loan');
		}
	};

	const requestClaim = async (loanId) => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.requestClaim(loanId).send({ from: accounts[0] });
			message.success(`Loan ${id} updated`);
			getLoans();
		} catch (err) {
			console.log(err);
			message.error('Error occured while updating Loan');
		}
	};

	const confirmRecivingOfClaim = async (loanId) => {
		try {
			const accounts = await window.ethereum.enable();
			await BankLoanContract.methods.confirmRecivingOfClaim(loanId).send({ from: accounts[0] });
			message.success(`Loan ${id} updated`);
			getLoans();
		} catch (err) {
			console.log(err);
			message.error('Error occured while updating Loan');
		}
	};

	const showModal = (value) => {
		setId(value);
		setIsModalVisible(true);
	};

	const showRejectModal = (value) => {
		setId(value);
		setIsRejectModalVisible(true);
	};

	const showInsuranceModal = (value) => {
		setId(value);
		setIsInsuranceModalVisible(true);
	};

	const handleOk = () => {
		approveLoan();
		setIsModalVisible(false);
	};

	const handleReject = () => {
		rejectLoan();
		setIsRejectModalVisible(false);
	};

	const handleInsurance = () => {
		addInsuranceToLoan();
		setIsInsuranceModalVisible(false);
	};

	const handleCancel = () => {
		setIsModalVisible(false);
		setIsRejectModalVisible(false);
		setIsInsuranceModalVisible(false);
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
			render: text => text,
		},
		// {
		// 	title: 'Borrower',
		// 	dataIndex: 'borrower',
		// 	key: 'borrower',
		// },
		// {
		// 	title: 'Broker',
		// 	dataIndex: 'broker',
		// 	key: 'broker',
		// },
		{
			title: 'Borrower Name',
			dataIndex: 'borrowerName',
			key: 'borrowerName',
		},
		{
			title: 'Broker Name',
			dataIndex: 'brokerName',
			key: 'brokerName',
		},
		{
			title: 'Amount',
			dataIndex: 'amount',
			key: 'amount',
		},
		{
			title: 'Period',
			dataIndex: 'period',
			key: 'period',
		},
		{
			title: 'Interest %',
			key: 'interest',
			dataIndex: 'interest',
		},
		{
			title: 'Plan ID',
			key: 'planId',
			dataIndex: 'planId',
		},
		{
			title: 'Status',
			key: 'status',
			dataIndex: 'status',
			render: tag => {
				let color = 'geekblue';
				if (tag === '4' || tag === '6' || tag === '10') {
					color = 'red';
				} else if (tag === '5' || tag === '9') {
					color = 'green';
				}
				return (
					<Tag color={color} key={tag}>
						{state[tag]}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'bank') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: 'x',
			render: (record) => {
				let actionBlock = '';
				if (record.status === '3') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => showModal(record.id)}>Approve</a>
							<Divider type="vertical" />
							<a href="javascript:;" onClick={() => showRejectModal(record.id)} style={{ color: 'red' }}>Reject</a>
						</span>;
				} else if (record.status === '5') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => confirmTokenTrasferToInsurance(record.id)}>
								Confirm Insurance Payment
							</a>
						</span>;
				} else if (record.status === '7') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => confirmTokenTrasferToBroker(record.id)}>
								Confirm Broker Payment
							</a>
						</span>;
				} else if (record.status === '8') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => confirmTokenTrasferToBorrower(record.id)}>
								Token Transferred to Borrower
							</a>
						</span>;
				} else if (record.status === '9') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => closeLoan(record.id)}>Close</a>
							<Divider type="vertical" />
							<a href="javascript:;" onClick={() => markAsDefaulted(record.id)} style={{ color: 'red' }}>Defaulted</a>
						</span>;
				} else if (record.status === '10') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => requestClaim(record.id)}>Claim Requested</a>
						</span>;
				} else if (record.status === '12') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => confirmRecivingOfClaim(record.id)}>Claimed</a>
						</span>;
				}
				return actionBlock;
			},
		});
	}

	if (user.role === 'broker') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: 'x',
			render: (record) => {
				let actionBlock = '';
				if (record.status === '0') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => showInsuranceModal(record.id)}>Add insurance</a>
						</span>;
				} else if (record.status === '1') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => notifyInsuranceApproval(record.id)}>Approved</a>
							<Divider type="vertical" />
							<a href="javascript:;" onClick={() => notifyInsuranceRejection(record.id)} style={{ color: 'red' }}>Rejected</a>
						</span>;
				}
				return actionBlock;
			},
		});
	}

	if (user.role === 'borrower') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: 'x',
			render: (record) => {
				if (record.status === '2') {
					return (
						<span>
							<a href="javascript:void(0);" onClick={() => signLoan(record.id)}>Sign Loan</a>
						</span>
					);
				}
			},
		});
	}

	const loanData = async () => {
		await getBrokers();
		await getBorrowers();
		await getPayments();
		await getLoans();
	};

	useEffect(() => {
		loanData();
		const emitter = BankLoanContract.events.loanRequest({ fromBlock: 'latest' }, (error, response) => {
			const result = response.returnValues;

			const row = {
				key: result.id,
				id: result.id,
				amount: result.amount,
				period: result.months,
				interest: result.interest,
				planId: result.planId,
				borrower: result.borrower,
				broker: result.broker,
				insurance: result.insurance,
				InsurancePolicyId: result.InsurancePolicyId,
				status: result.state,
			};

			// console.log(row)

			setData((prev) => {
				return [...prev, row];
			});
		});

		return () => {
			emitter.unsubscribe();
		};
	}, []);

	const expandedRowRender = (record) => {
		console.log(record);

		const expandedData = [];

		expandedData.push(record);

		const expandedPayments = payments.filter(item => item.loanId == record.id);

		const expandedPaymentColumns = [
			{ title: 'Payment ID', dataIndex: '_id', key: 'id' },
			{ title: 'Amount', dataIndex: 'amount', key: 'amount' },
			{ title: 'Loan ID', dataIndex: 'loanId', key: 'loanId' },
			{ title: 'Transaction Hash', dataIndex: 'transactionHash', key: 'transactionHash' },
		];

		return (
			<>
				<Form
					labelCol={{
						lg: 6,
						xl: 6,
						xxl: 3,
					}}
					wrapperCol={{
						lg: 12,
						xl: 12,
						xxl: 16,
					}}
					layout="horizontal"
					initialValues={{
						size: componentSize,
					}}
					size={componentSize}
					labelAlign="left"
				>
					<Form.Item label="Borrower address">
						<span>{record.borrower}</span>
					</Form.Item>
					<Form.Item label="Broker address">
						<span>{record.broker}</span>
					</Form.Item>
				</Form>
				<Table
					columns={expandedPaymentColumns}
					dataSource={expandedPayments}
					pagination={false}
				/>
			</>
		);
	};

	return (
		<>
			<Card title="Current Loans">
				<Table
					pagination="true"
					columns={columns}
					dataSource={data}
					expandable={{
						expandedRowRender,
					}}
				/>
			</Card>
			<Modal title={`Approve Loan Request ${id}`} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
				<p>Confirm?</p>
			</Modal>
			<Modal title={`Reject Loan Request ${id}`} visible={isRejectModalVisible} onOk={handleReject} onCancel={handleCancel}>
				<p>Reject loan request?</p>
			</Modal>
			<Modal
				title={`Add Insurance - Loan Request ${id}`}
				visible={isInsuranceModalVisible}
				onOk={handleInsurance}
				onCancel={handleCancel}
				width="50%"
			>
				<Form
					labelCol={{
						lg: 6,
						xl: 6,
						xxl: 5,
					}}
					wrapperCol={{
						lg: 12,
						xl: 12,
						xxl: 16,
					}}
					layout="horizontal"
					initialValues={{
						size: componentSize,
					}}
					onValuesChange={onFormLayoutChange}
					size={componentSize}
					labelAlign="left"
				>
					<Form.Item label="Loan ID">
						<Input
							placeholder="Enter Insurance Wallet Address"
							value={id}
							disabled="true"
						/>
					</Form.Item>
					<Form.Item label="Insurance Address">
						<Input
							placeholder="Enter Insurance Wallet Address"
							value={insurance}
							onChange={(e) => setInsurance(e.target.value)}
						/>
					</Form.Item>
					<Form.Item label="Insurance Policy ID">
						<InputNumber
							min="0"
							style={{ width: '100%' }}
							placeholder="Enter Insurance Policy Id"
							value={insuranceId}
							onChange={(e) => setInsuranceId(e)}
						/>
					</Form.Item>
				</Form>
			</Modal>
		</>
	);
}

export default LoansTable;
