import React, { useState, useContext } from 'react';
import { Card, Form, Input, Button, message, Upload, Image } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import AuthContext from '../../stores/authContext';

function CreateBorrowerForm() {
	const [componentSize, setComponentSize] = useState('default');
	const [form] = Form.useForm();

	const { UserIdentityContract } = useContext(AuthContext);
	const PLEASE_UPLOAD_DOC = 'Please upload the document';
	const API_URL = process.env.API_URL;

	const [ipfsHash, setIpfsHash] = useState(PLEASE_UPLOAD_DOC);

	const onFormLayoutChange = ({ size }) => {
		setComponentSize(size);
	};

	const createBorrower = async (values) => {
		try {
			const accounts = await window.ethereum.enable();
			console.log(values);
			console.log(accounts);
			await UserIdentityContract.methods.addBorrower(values.socialId, values.address, values.name, values.ipfsHash).send({ from: accounts[0] });
			// console.log(response)
			message.success('New Borrower registration form submitted successfully!');
		} catch (err) {
			message.error('Error occured while Borrower registration');
			console.log(err);
		}
	};

	const props = {
		name: 'file',
		action: API_URL + 'upload',
		headers: {
			authorization: 'authorization-text',
		},
		maxCount: 1,
		onChange(info) {
			if (info.file.status !== 'uploading') {
				console.log(info.file, info.fileList);
			}
			if (info.file.status === 'done') {
				message.success(`${info.file.name} file uploaded successfully`);
				console.log(info);
				setIpfsHash(info.file.response);
				form.setFieldsValue({
					ipfsHash: info.file.response,
				});
			} else if (info.file.status === 'error') {
				message.error(`${info.file.name} file upload failed.`);
			}
		},
	};

	return (

		<Card title="Borrower Application">
			<Form
				form={form}
				labelCol={{
					lg: 4,
					xl: 3,
					xxl: 2,
				}}
				wrapperCol={{
					lg: 16,
					xl: 14,
					xxl: 10,
				}}
				layout="horizontal"
				initialValues={{
					size: componentSize,
				}}
				onValuesChange={onFormLayoutChange}
				size={componentSize}
				labelAlign="left"
				onFinish={createBorrower}
			>
				<Form.Item label="Id Number" name="socialId" rules={[{ required: true, message: 'Please input Borrower\'s social security id!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter social security number"
					/>
				</Form.Item>
				<Form.Item label="Borrower Name" name="name" rules={[{ required: true, message: 'Please input Borrower\'s name!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter borrower's name"
					/>
				</Form.Item>
				<Form.Item label="Wallet Address" name="address" rules={[{ required: true, message: 'Please input Borrower\'s address!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter borrower's wallet address"
					/>
				</Form.Item>
				<Form.Item label="Documents">
					<Upload {...props}>
						<Button icon={<UploadOutlined />}>Click to Upload</Button>
					</Upload>
				</Form.Item>
				<Form.Item label="Document Hash" name="ipfsHash" rules={[{ required: true, message: 'Please enter IPFS documents hash!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter IPFS Hash value"
						onChange={(e) => setIpfsHash(e.target.value)}
					/>
				</Form.Item>
				{
					PLEASE_UPLOAD_DOC !== ipfsHash &&
					<Form.Item label="Document">
						<Image
							width={200}
							src={`https://ipfs.io/ipfs/${ipfsHash}`}
						/>
					</Form.Item>
				}
				<Form.Item wrapperCol={{
					lg: { span: 14, offset: 4 },
					xl: { span: 14, offset: 3 },
					xxl: { span: 14, offset: 2 } }}
				>
					<Button type="primary" htmlType="submit">Register Borrower</Button>
				</Form.Item>
			</Form>
		</Card>
	);
}

export default CreateBorrowerForm;
