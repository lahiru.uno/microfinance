import React, { useState, useContext } from 'react';
import { Card, Form, Input, Button, message, Upload, Image } from 'antd';
import { UploadOutlined, AimOutlined } from '@ant-design/icons';
import AuthContext from '../../stores/authContext';

function BrokerRegistrationForm() {
	const [componentSize, setComponentSize] = useState('default');
	const [form] = Form.useForm();

	const { UserIdentityContract } = useContext(AuthContext);

	const PLEASE_UPLOAD_DOC = 'Please upload the document';
	const API_URL = process.env.API_URL;

	// const [address, setAddress] = useState('');
	// const [name, setName] = useState('');
	// const [socialId, setSocialId] = useState('');
	const [ipfsHash, setIpfsHash] = useState(PLEASE_UPLOAD_DOC);

	const onFormLayoutChange = ({ size }) => {
		setComponentSize(size);
	};

	const createBroker = async (values) => {
		try {
			const accounts = await window.ethereum.enable();

			await UserIdentityContract.methods.addBroker(values.socialId, values.address, values.name, values.ipfsHash).send({ from: accounts[0] });
			message.success('Registered as Broker successfully');
		} catch (err) {
			message.error('Error registering as Broker');
			console.log(err);
		}
	};

	const props = {
		name: 'file',
		action: API_URL + 'upload',
		headers: {
			authorization: 'authorization-text',
		},
		maxCount: 1,
		onChange(info) {
			if (info.file.status !== 'uploading') {
				console.log(info.file, info.fileList);
			}

			if (info.file.status === 'done') {
				message.success(`${info.file.name} file uploaded successfully`);
				console.log(info);
				setIpfsHash(info.file.response);
				form.setFieldsValue({
					ipfsHash: info.file.response,
				});
			} else if (info.file.status === 'error') {
				message.error(`${info.file.name} file upload failed.`);
			}
		},
	};

	const setWalletAddress = async () => {
		const accounts = await window.ethereum.enable();
		form.setFieldsValue({
			address: accounts[0],
		});
		// setAddress(accounts[0]);
	};

	return (

		<Card title="Broker Registration Form">
			<Form
				labelCol={{
					lg: 4,
					xl: 3,
					xxl: 2,
				}}
				wrapperCol={{
					lg: 16,
					xl: 14,
					xxl: 10,
				}}
				layout="horizontal"
				initialValues={{
					size: componentSize,
				}}
				onValuesChange={onFormLayoutChange}
				size={componentSize}
				labelAlign="left"
				onFinish={createBroker}
				form={form}
			>
				<Form.Item label="Id Number" name="socialId" rules={[{ required: true, message: 'Please enter social security id!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter social security number"
					/>
				</Form.Item>
				<Form.Item label="Broker Name" name="name" rules={[{ required: true, message: 'Please enter name!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter broker's name"
					/>
				</Form.Item>
				<Form.Item label="Wallet Address" name="address" rules={[{ required: true, message: 'Please enter wallet address!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter borrower's wallet address"
						addonAfter={<AimOutlined onClick={(e) => setWalletAddress(e)} />}
					/>
				</Form.Item>
				<Form.Item label="Documents">
					<Upload {...props}>
						<Button icon={<UploadOutlined />}>Click to Upload</Button>
					</Upload>
				</Form.Item>
				<Form.Item label="Document Hash" name="ipfsHash" rules={[{ required: true, message: 'Please enter IPFS documents hash!' }]}>
					{/* <span className="ant-form-text">{ipfsHash}</span> */}
					<Input
						style={{ width: '100%' }}
						placeholder="Enter IPFS Hash value"
						onChange={(e) => setIpfsHash(e.target.value)}
					/>
				</Form.Item>
				{
					PLEASE_UPLOAD_DOC !== ipfsHash &&
					<Form.Item label="Document">
						<Image
							width={200}
							src={`https://ipfs.io/ipfs/${ipfsHash}`}
						/>
					</Form.Item>
				}

				<Form.Item wrapperCol={{
					lg: { span: 14, offset: 4 },
					xl: { span: 14, offset: 3 },
					xxl: { span: 14, offset: 2 } }}
				>
					<Button type="primary" htmlType="submit">Register as Broker</Button>
				</Form.Item>
			</Form>
		</Card>
	);
}

export default BrokerRegistrationForm;
