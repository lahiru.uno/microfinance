import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, Divider, message, Modal, Image } from 'antd';
import AuthContext from '../../stores/authContext';

function BorrowersTable() {
	const { user, UserIdentityContract } = useContext(AuthContext);

	const [isDocumentsModalVisible, setIsDocumentsModalVisible] = useState(false);
	const [id, setId] = useState(-1);
	const [currentRecord, setCurrentRecord] = useState('');
	const [data, setData] = useState([]);
	const brokers = {};

	const getBrokers = async () => {
		const response = await UserIdentityContract.methods.getAllBrokers().call();
		for (let i = 0; i < response.length; i++) {
			brokers[response[i].userAddress] = response[i].name;
		}
	};

	const getBorrowers = async () => {
		try {
			const response = await UserIdentityContract.methods.getAllBorrowers().call();

			setData([]);

			for (let i = 0; i < response.length; i++) {
				const row = {
					key: response[i].id,
					id: response[i].id,
					socialId: response[i].socialSecurityId,
					address: response[i].userAddress,
					name: response[i].name,
					addedBy: brokers[response[i].addedBy],
					documentsUri: response[i].documentsUri,
					status: response[i].state,
				};

				console.log(row);

				setData((prev) => {
					return [...prev, row];
				});
			}
			// console.log(response);
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading brokers');
		}
	};

	const handleCancel = () => {
		setIsDocumentsModalVisible(false);
	};

	const showDocumentsModal = (record) => {
		setId(record.id);
		setCurrentRecord(record);
		setIsDocumentsModalVisible(true);
	};

	const handleDocumentsModalOk = () => {
		setIsDocumentsModalVisible(false);
	};

	const approveBorrower = async (borrowerAddress) => {
		try {
			const accounts = await window.ethereum.enable();
			await UserIdentityContract.methods.approveBorrower(borrowerAddress).send({ from: accounts[0] });
			message.success(`Borrower ${borrowerAddress} approved`);
			getBorrowers();
		} catch (err) {
			message.error('Error occured while approving Borrower');
			console.log(err);
		}
	};

	const rejectBorrower = async (borrowerAddress) => {
		try {
			const accounts = await window.ethereum.enable();
			await UserIdentityContract.methods.rejectBorrower(borrowerAddress).send({ from: accounts[0] });
			message.success(`Borrower ${borrowerAddress} rejected`);
			getBorrowers();
		} catch (err) {
			message.error('Error occured while rejecting Borrower');
		}
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
			render: text => text,
		},
		{
			title: 'Social Id',
			dataIndex: 'socialId',
			key: 'socialId',
		},
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Address',
			dataIndex: 'address',
			key: 'address',
		},
		{
			title: 'Added by',
			dataIndex: 'addedBy',
			key: 'addedBy',
		},
		{
			title: 'Document',
			key: '',
			dataIndex: '',
			render: (record) => (
				<span>
					<a href="javascript:void(0);" onClick={() => showDocumentsModal(record)}>View</a>
				</span>
			),
		},
		{
			title: 'Status',
			key: 'status',
			dataIndex: 'status',
			render: tag => {
				const s = ['PENDING', 'APPROVED', 'REJECTED'];
				const c = ['geekblue', 'green', 'red'];
				return (
					<Tag color={c[tag]} key={tag}>
						{s[tag]}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'bank') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: 'x',
			render: (record) => (
				record.status === '0' ?
					<span>
						<a href="javascript:void(0);" onClick={() => approveBorrower(record.address)}>Approve</a>
						<Divider type="vertical" />
						<a href="javascript:;" style={{ color: 'red' }} onClick={() => rejectBorrower(record.address)}>Reject</a>
					</span> : null
			),
		});
	}

	useEffect(() => {
		getBrokers();
		getBorrowers();
	}, []);

	return (
		<>
			<Card title="Borrowers">
				<Table pagination="true" columns={columns} dataSource={data} />
			</Card>
			<Modal title={`Documents - User Id  ${id}`} visible={isDocumentsModalVisible} onOk={handleDocumentsModalOk} onCancel={handleCancel}>
				<Image
					width={400}
					src={`https://ipfs.io/ipfs/${currentRecord.documentsUri}`}
					preview={false}
				/>
			</Modal>
		</>
	);
}

export default BorrowersTable;
