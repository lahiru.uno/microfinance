import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, Divider, message, Modal, Image } from 'antd';
import AuthContext from '../../stores/authContext';

function BrokersTable() {
	const { user, UserIdentityContract } = useContext(AuthContext);
	const [data, setData] = useState([]);

	const documentsInfo = (record) => {
		Modal.info({
			width: 500,
			title: `Documents - User Id  ${record.id}`,
			content: <Image
				width={400}
				src={`https://ipfs.io/ipfs/${record.documentsUri}`}
				preview={false}
			/>,
		});
	};

	const getBrokers = async () => {
		try {
			const response = await UserIdentityContract.methods.getAllBrokers().call();

			setData([]);

			for (let i = 0; i < response.length; i++) {
				const row = {
					key: response[i].id,
					id: response[i].id,
					socialId: response[i].socialSecurityId,
					address: response[i].userAddress,
					name: response[i].name,
					documentsUri: response[i].documentsUri,
					status: response[i].state,
				};

				setData((prev) => {
					return [...prev, row];
				});
			}
		} catch (err) {
			message.error('Error occured while loading brokers');
		}
	};

	const approveBroker = async (address) => {
		try {
			const accounts = await window.ethereum.enable();
			await UserIdentityContract.methods.approveBroker(address).send({ from: accounts[0] });
			message.success('User approved successfully!');
			getBrokers();
		} catch (err) {
			message.error('Error approving user');
		}
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
			render: text => text,
		},
		{
			title: 'Social Id',
			dataIndex: 'socialId',
			key: 'socialId',
		},
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Address',
			dataIndex: 'address',
			key: 'address',
		},
		{
			title: 'Document',
			key: '',
			dataIndex: '',
			render: (record) => (
				<span>
					<a href="javascript:void(0);" onClick={() => documentsInfo(record)}>View</a>
				</span>
			),
		},
		{
			title: 'Status',
			key: 'status',
			dataIndex: 'status',
			render: tag => {
				/* PENDING, APPROVED, REJECTED */
				let color = '';
				let state = '';
				if (tag === '0') {
					color = 'geekblue';
					state = 'PENDING';
				} else if (tag === '1') {
					color = 'green';
					state = 'APPROVED';
				} else {
					color = 'red';
					state = 'REJECTED';
				}
				return (
					<Tag color={color} key={tag}>
						{state}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'bank') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: 'x',
			render: (record) => (
				record.status === '0' ?
					<span>
						<a href="javascript:void(0);" onClick={() => approveBroker(record.address)}>Approve</a>
						<Divider type="vertical" />
						<a href="javascript:;" style={{ color: 'red' }}>Reject</a>
					</span> : null
			),
		});
	}

	useEffect(() => {
		getBrokers();
		// TODO: add event listner for newUserAdded event.
	}, []);

	return (
		<>
			<Card title="Brokers">
				<Table pagination={true} columns={columns} dataSource={data} />
			</Card>
		</>
	);
}

export default BrokersTable;
