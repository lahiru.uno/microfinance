import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, Divider, message, Modal, Image } from 'antd';
import AuthContext from '../../stores/authContext';

function InsuranceCompanyTable() {
	const { user, UserIdentityContract } = useContext(AuthContext);

	const [data, setData] = useState([]);

	const documentsInfo = (record) => {
		Modal.info({
			width: 500,
			title: `Documents - User Id  ${record.id}`,
			content: <Image
				width={400}
				src={`https://ipfs.io/ipfs/${record.documentsUri}`}
				preview={false}
			/>,
		});
	};

	const getInsuranceCompanies = async () => {
		try {
			const response = await UserIdentity.methods.getAllInsurers().call();

			setData([]);

			for (let i = 0; i < response.length; i++) {
				const row = {
					key: response[i].id,
					id: response[i].id,
					regNumber: response[i].registrationNumber,
					address: response[i].walletAddress,
					name: response[i].name,
					status: response[i].state,
				};

				setData((prev) => {
					return [...prev, row];
				});
			}
		} catch (err) {
			message.error('Error occured while loading brokers');
		}
	};

	const approveInsuranceCompany = async (address) => {
		try {
			const accounts = await window.ethereum.enable();
			await UserIdentityContract.methods.approveInsuranceCompany(address).send({ from: accounts[0] });
			message.success('Insurance Company approved successfully!');
			getInsuranceCompanies();
		} catch (err) {
			message.error('Error approving Insurance Company');
		}
	};

	const rejectInsuranceCompany = async (address) => {
		try {
			const accounts = await window.ethereum.enable();
			await UserIdentityContract.methods.rejectInsuranceCompany(address).send({ from: accounts[0] });
			message.success(`Insurance Company ${address} rejected`);
			getInsuranceCompanies();
		} catch (err) {
			message.error('Error occured while rejecting Insurance Company');
		}
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
			render: text => text,
		},
		{
			title: 'Registration Number',
			dataIndex: 'regNumber',
			key: 'regNumber',
		},
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Address',
			dataIndex: 'address',
			key: 'address',
		},
		{
			title: 'Status',
			key: 'status',
			dataIndex: 'status',
			render: tag => {
				/* PENDING, APPROVED, REJECTED */
				let color = '';
				let state = '';
				if (tag === '0') {
					color = 'geekblue';
					state = 'PENDING';
				} else if (tag === '1') {
					color = 'green';
					state = 'APPROVED';
				} else {
					color = 'red';
					state = 'REJECTED';
				}
				return (
					<Tag color={color} key={tag}>
						{state}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'bank') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: 'x',
			render: (record) => (
				record.status === '0' ?
					<span>
						<a href="javascript:void(0);" onClick={() => approveInsuranceCompany(record.address)}>Approve</a>
						<Divider type="vertical" />
						<a href="javascript:;" style={{ color: 'red' }} onClick={() => rejectInsuranceCompany(record.address)}>Reject</a>
					</span> : null
			),
		});
	}

	useEffect(() => {
		getInsuranceCompanies();
		// TODO: add event listner for newUserAdded event.
	}, []);

	return (
		<>
			<Card title="Insurance Companies">
				<Table pagination="True" columns={columns} dataSource={data} />
			</Card>
		</>
	);
}

export default InsuranceCompanyTable;
