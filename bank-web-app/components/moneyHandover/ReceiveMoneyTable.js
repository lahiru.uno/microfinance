import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, Divider, message, Modal, Form, InputNumber } from 'antd';
import AuthContext from '../../stores/authContext';

function ReceiveMoneyTable() {
	const { user, UserIdentityContract, LoanPaymentContract } = useContext(AuthContext);

	const [isModalVisible, setIsModalVisible] = useState(false);
	const [isRejectModelVisible, setIsRejectModelVisible] = useState(false);
	const [isConfirmModelVisible, setIsConfirmModelVisible] = useState(false);
	const [isFlagModelVisible, setIsFlagModelVisible] = useState(false);
	const [data, setData] = useState([]);
	const [id, setId] = useState(-1);
	const [pin, setPin] = useState('');
	const borrowers = {};
	const brokers = {};

	const showModal = (value) => {
		setId(value);
		setIsModalVisible(true);
	};

	const showRejectModal = (value) => {
		setId(value);
		setIsRejectModelVisible(true);
	};

	const showConfirmModal = (value) => {
		setId(value);
		setIsConfirmModelVisible(true);
	};

	const showFlagModal = (value) => {
		setId(value);
		setIsFlagModelVisible(true);
	};

	const handleCancel = () => {
		setIsModalVisible(false);
	};

	const handleRejectCancel = () => {
		setIsRejectModelVisible(false);
	};

	const handleConfirmCancel = () => {
		setIsConfirmModelVisible(false);
	};

	const handleFlagCancel = () => {
		setIsFlagModelVisible(false);
	};

	const getAllBorrowers = async () => {
		const response = await UserIdentityContract.methods.getAllBorrowers().call();
		for (let i = 0; i < response.length; i++) {
			borrowers[response[i].userAddress] = response[i].name;
		}
	};

	const getBrokers = async () => {
		const response = await UserIdentityContract.methods.getAllBrokers().call();
		for (let i = 0; i < response.length; i++) {
			brokers[response[i].userAddress] = response[i].name;
		}
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
			render: text => text,
		},
		{
			title: 'Borrower',
			dataIndex: 'borrower',
			key: 'borrower',
		},
		{
			title: 'Broker',
			dataIndex: 'broker',
			key: 'broker',
		},
		{
			title: 'Loan Id',
			dataIndex: 'loanId',
			key: 'loanId',
		},
		{
			title: 'Token Amount',
			dataIndex: 'tokenAmount',
			key: 'tokenAmount',
		},
		{
			title: 'Amount',
			key: 'amount',
			dataIndex: 'amount',
		},
		{
			title: 'Status',
			key: 'status',
			dataIndex: 'status',
			render: tag => {
				/* HANDSHAKE_START, HANDSHAKE_SUCCESS, MONEY_HANDEDOVER, MONEY_RECEIVED, FLAG_TRANSACTION, CANCEL */
				let color = '';
				let state = '';
				if (tag === '0') {
					color = 'geekblue';
					state = 'HNDSHAKE START';
				} else if (tag === '1') {
					color = 'green';
					state = 'HANDSHAKE SUCCESS';
				} else if (tag === '2') {
					color = 'geekblue';
					state = 'MONEY HANDOVER';
				} else if (tag === '3') {
					color = 'green';
					state = 'MONEY RECEIVED';
				} else if (tag === '4') {
					color = 'red';
					state = 'FLAG TRANSACTION';
				} else {
					color = 'red';
					state = 'CANCELLED';
				}
				return (
					<Tag color={color} key={tag}>
						{state}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'broker') {
		// columns.splice(1, 0, {
		// 	title: 'Name',
		// 	dataIndex: 'name',
		// 	key: 'name',
		// });

		columns.push({
			title: 'Action',
			dataIndex: '',
			key: 'x',
			render: (record) => (
				<>
					{ 	record.status === '0' &&
						<span>
							<a href="javascript:void(0);" onClick={() => showModal(record.id)}>Approve</a>
							<Divider type="vertical" />
							<a href="javascript:;" style={{ color: 'red' }} onClick={() => showRejectModal(record.id)}>Reject</a>
						</span>}
					{
						record.status === '2' &&
						<span>
							<a href="javascript:void(0);" onClick={() => showConfirmModal(record.id)}>Confirm money</a>
							<Divider type="vertical" />
							<a href="javascript:;" style={{ color: 'red' }} onClick={() => showFlagModal(record.id)}>Flag payment</a>
						</span>
					}
				</>
			),
		});
	}

	const getLoanPayments = async () => {
		try {
			getAllBorrowers();
			getBrokers();
			const response = await LoanPaymentContract.methods.getAllPayments().call();

			setData([]);

			for (let i = 0; i < response.length; i++) {
				const row = {
					key: response[i].id,
					id: response[i].id,
					pin: response[i].pin,
					borrower: borrowers[response[i].borrower] || response[i].borrower,
					broker: brokers[response[i].broker] || response[i].broker,
					brokerAddress: response[i].broker,
					borrowerAddress: response[i].borrower,
					loanId: response[i].loanId,
					tokenAmount: response[i].tokenAmount,
					amount: response[i].amount,
					status: response[i].status,
				};

				setData((prev) => {
					return [...prev, row];
				});
			}
		} catch (err) {
			message.error('Error occured while loading payments');
		}
	};

	const confirmHandshake = async () => {
		try {
			const accounts = await window.ethereum.enable();
			// console.log(accounts[0]);
			await LoanPaymentContract.methods.confirmHandshake(id, pin).send({ from: accounts[0] });
			message.success(`Handshake confirm for payment id ${id}`);
			getLoanPayments();
			setPin('');
		} catch (err) {
			setPin('');
			message.error('Error occured while confirming handshake');
		}
	};

	const rejectHandshake = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await LoanPaymentContract.methods.cancelByBroker(id).send({ from: accounts[0] });
			message.success(`Handshake rejected for payment id ${id}`);
			getLoanPayments();
		} catch (err) {
			message.error('Error occured while rejecting handshake');
		}
	};

	const confirmMoney = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await LoanPaymentContract.methods.confirmMoneyReceived(id).send({ from: accounts[0] });
			message.success('Successfully confirmed the money');
			getLoanPayments();
		} catch (err) {
			message.error('Error occured while confirming money');
		}
	};

	const flagMoneyHandover = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await LoanPaymentContract.methods.flagByBroker(id).send({ from: accounts[0] });
			message.success(`Payment id ${id} is successfully flaged`);
			getLoanPayments();
		} catch (err) {
			message.error('Error occured while flagging the payment');
		}
	};

	useEffect(() => {
		getLoanPayments();

		const paymentHandshakeEmitter = LoanPaymentContract.events.paymentHandshake({ fromBlock: 'latest' }, (error, response) => {
			const result = response.returnValues;
			setData((prev) => {
				console.log(prev);
				const payment = {
					key: result.id,
					id: result.id,
					pin: result.pin,
					borrower: borrowers[result.borrower] || result.borrower,
					broker: brokers[result.broker] || result.broker,
					brokerAddress: result.broker,
					borrowerAddress: result.borrower,
					loanId: result.loanId,
					tokenAmount: result.tokenAmount,
					amount: result.amount,
					status: result.status,

				};
				return [...prev, payment];
			});
		});

		const borrowerFlagEmitter = LoanPaymentContract.events.flagPayment({ fromBlock: 'latest' }, (error, response) => {
			getLoanPayments();
		});

		const moneyHandoverEmitter = LoanPaymentContract.events.moneyHandover({ fromBlock: 'latest' }, (error, response) => {
			getLoanPayments();
		});

		return () => {
			paymentHandshakeEmitter.unsubscribe();
			borrowerFlagEmitter.unsubscribe();
			moneyHandoverEmitter.unsubscribe();
			console.log('Cleanup functions');
		};
	}, []);

	const handleOk = () => {
		confirmHandshake();
		setIsModalVisible(false);
	};

	const handleRejectOk = () => {
		rejectHandshake();
		setIsRejectModelVisible(false);
	};

	const handleConfirmOk = () => {
		confirmMoney();
		setIsConfirmModelVisible(false);
	};

	const handleFlagOk = () => {
		flagMoneyHandover();
		setIsFlagModelVisible(false);
	};

	const expandedRowRender = (record) => {
		console.log(record);

		const expandedData = [];

		expandedData.push(record);

		const expandedColumns = [
			{ title: 'Borrower Address', dataIndex: 'borrowerAddress', key: 'borrowerAddress', width: '30%' },
			{ title: 'Broker Address', dataIndex: 'brokerAddress', key: 'brokerAddress', width: '30%' },
		];

		return (
			<>
				<Table
					columns={expandedColumns}
					dataSource={expandedData}
					pagination={false}
				/>
			</>
		);
	};

	return (
		<>
			<Card title="Current Loan Payments">
				<Table
					pagination="true"
					columns={columns}
					dataSource={data}
					expandable={{
						expandedRowRender,
					}}
				/>
			</Card>
			<Modal title={`Confirm handshake - Payment Id: ${id}`} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
				<Form
					labelCol={{
						lg: 5,
						xl: 4,
						xxl: 3,
					}}
					wrapperCol={{
						lg: 14,
						xl: 12,
						xxl: 10,
					}}
					layout="horizontal"
					initialValues={{
						size: 'default',
					}}
					size="default"
					labelAlign="left"
				>
					<Form.Item label="PIN">
						<InputNumber
							min="0"
							max="9999"
							style={{ width: '100%' }}
							value={pin}
							onChange={(e) => setPin(e)}
							placeholder="Enter 4 digit PIN"
						/>
					</Form.Item>
				</Form>

			</Modal>
			<Modal title={`Reject handshake - Payment Id: ${id}`} visible={isRejectModelVisible} onOk={handleRejectOk} onCancel={handleRejectCancel}>
				{`Are you sure you want to reject handshake for Payment ${id}`}
			</Modal>
			<Modal title={`Confirm money - Payment Id: ${id}`} visible={isConfirmModelVisible} onOk={handleConfirmOk} onCancel={handleConfirmCancel}>
				{`Confirm you received money for Payment ${id}`}
			</Modal>
			<Modal title={`Flag money handover- Payment Id: ${id}`} visible={isFlagModelVisible} onOk={handleFlagOk} onCancel={handleFlagCancel}>
				{`Are you sure you want to flag money handover for Payment ${id}`}
			</Modal>
		</>
	);
}

export default ReceiveMoneyTable;
