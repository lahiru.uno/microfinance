import React, { useState } from 'react';
import { Button, Form, Space, Input, Typography } from 'antd';

function MoneyHandoverConfirm({ paymentId, broker, amount, tokens, loanId, handoverMoney, cancelByBorrower }) {
	const [componentSize] = useState('default');
	const { Title } = Typography;

	return (
		<>
			<Title level={5}>Please handover amount of ${amount} to Broker and Record it</Title>
			<Form
				labelCol={{
					lg: 5,
					xl: 4,
					xxl: 3,
				}}
				wrapperCol={{
					lg: 14,
					xl: 12,
					xxl: 10,
				}}
				layout="horizontal"
				initialValues={{
					size: componentSize,
				}}
				size={componentSize}
				labelAlign="left"
			>
				<Form.Item label="Payment Id">
					<Input
						disabled="True"
						value={paymentId}
					/>
				</Form.Item>
				<Form.Item label="Broker">
					<Input
						disabled="True"
						value={broker}
					/>
				</Form.Item>
				<Form.Item label="Loan Id">
					<Input
						disabled="True"
						value={loanId}
					/>
				</Form.Item>
				<Form.Item label="Amount">
					<Input
						disabled="True"
						value={amount}
					/>
				</Form.Item>
				<Form.Item label="Token Amount">
					<Input
						disabled="True"
						value={tokens}
					/>
				</Form.Item>
				<Form.Item wrapperCol={{
					lg: { span: 14, offset: 5 },
					xl: { span: 14, offset: 4 },
					xxl: { span: 14, offset: 3 } }}
				>
					<Space direction="horizontal">
						<Button type="primary" onClick={(e) => handoverMoney(e)}>Record Money Handover</Button>
						<Button onClick={(e) => cancelByBorrower(e)}>Cancel Money Handover</Button>
					</Space>
				</Form.Item>
			</Form>
		</>

	);
}

export default MoneyHandoverConfirm;
