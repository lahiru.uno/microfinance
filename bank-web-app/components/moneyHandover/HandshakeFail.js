import React from 'react';
import { Result, Button } from 'antd';

function HandshakeFail({ backToHome }) {
	return (
		<Result
			status="error"
			title="Handshake unsuccessful!"
			subTitle="Please go back and try again"
			extra={[
				<Button type="primary" key="next" onClick={(e) => backToHome(e)}>
					Back
				</Button>,
			]}
		/>

	);
}

export default HandshakeFail;
