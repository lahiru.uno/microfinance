import React, { useEffect, useState, useContext } from 'react';
import { Typography, Card, Divider, message, Steps, Col, Row, Popover } from 'antd';
import AuthContext from '../../stores/authContext';
import MoneyHandoverForm from './MoneyHandoverForm';
import HandshakeWaitingForm from './HandshakeWaitingForm';
import HandshakeSuccess from './HandshakeSuccess';
import HandshakeFail from './HandshakeFail';
import MoneyHandoverConfirm from './MoneyHandoverConfirm';
import BrokerConfirmationWaiting from './BrokerConfirmationWaiting';
import CancelByBorrower from './CancelByBorrower';
import BrokerConfirmationFail from './BrokerConfirmationFail';
import BrokerConfirmationSuccess from './BrokerConfirmationSuccess';

const { Title } = Typography;
const { Step } = Steps;

function MoneyHandoverBorrowerController() {
	const [loanId, setLoanId] = useState('');
	const [tokens, setTokens] = useState('');
	const [broker, setBroker] = useState('');
	const [amount, setAmount] = useState('');
	const [pin, setPin] = useState('');
	const { LoanPaymentContract } = useContext(AuthContext);
	const [currentPaymentId, setCurrentPaymentId] = useState('');
	const [isHandshakeConfirm, setIsHandshakeConfirm] = useState(false);
	const [isHandshakeReject, setIsHandshakeReject] = useState(false);
	const [handshakeConfirmPaymentId, setHandshakeConfirmPaymentId] = useState('');
	const [handshakeRejectPaymentId, setHandshakeRejectPaymentId] = useState('');
	const [isTimesUp, setIsTimesUp] = useState(false);
	const [isBrokerConfirm, setIsBrokerConfirm] = useState(false);
	const [isBrokerReject, setIsBrokerReject] = useState(false);
	const [moneyConfirmPaymentId, setMoneyConfirmPaymentId] = useState('');
	const TOKEN_VALUE = 4;

	const [current, setCurrent] = React.useState(0);

	const next = () => {
		setCurrent(current + 1);
	};

	const setDefaultStates = () => {
		setIsHandshakeConfirm(false);
		setIsHandshakeReject(false);
		setCurrentPaymentId('');
		setHandshakeConfirmPaymentId('');
		setHandshakeRejectPaymentId('');
		setMoneyConfirmPaymentId('');
		setIsBrokerConfirm('');
		setIsBrokerReject('');
		setIsTimesUp('');
		setPin('');
	};

	const backToHome = () => {
		setCurrent(0);
		setDefaultStates();
	};

	const startHandshake = async () => {
		try {
			const accounts = await window.ethereum.enable();
			console.log(loanId);
			const response = await LoanPaymentContract.methods.startHandshake(loanId, amount, tokens, broker, pin).send({
				from: accounts[0] });
			console.log(response);
			console.log(response.events.paymentHandshake.returnValues);
			setCurrent(current + 1);
			setCurrentPaymentId(response.events.paymentHandshake.returnValues.id);
			message.success('Handshake started. Wait for broker response.');
		} catch (err) {
			console.log(err);
			message.error('Error occured while starting handshake');
		}
	};

	const handoverMoney = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await LoanPaymentContract.methods.handoverMoney(currentPaymentId).send({
				from: accounts[0] });
			setCurrent(current + 1);
			message.success('Money handover recorded successfully');
		} catch (err) {
			console.log(err);
			message.error('Error occured while recording money handover');
		}
	};

	const cancelByBorrower = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await LoanPaymentContract.methods.cancelByBorrower(currentPaymentId).send({
				from: accounts[0] });
			backToHome();
			message.success('Successfully cancel the money handover');
		} catch (err) {
			console.log(err);
			message.error('Error occured while cancelling money handover');
		}
	};

	const flagByBorrower = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await LoanPaymentContract.methods.flagByBorrower(currentPaymentId).send({
				from: accounts[0] });
			backToHome();
			message.success('Query submitted successfully');
		} catch (err) {
			console.log(err);
			message.error('Error occured while submitting the query');
		}
	};

	useEffect(() => {
		if (pin !== '') {
			startHandshake();
		}
	}, [pin]);

	useEffect(() => {
		if (handshakeConfirmPaymentId !== '' && handshakeConfirmPaymentId === currentPaymentId) {
			setIsHandshakeConfirm(true);
		}
	}, [handshakeConfirmPaymentId]);

	useEffect(() => {
		if (handshakeRejectPaymentId !== '' && handshakeRejectPaymentId === currentPaymentId) {
			setIsHandshakeReject(true);
		}
	}, [handshakeRejectPaymentId]);

	useEffect(() => {
		if (moneyConfirmPaymentId !== '' && moneyConfirmPaymentId === currentPaymentId) {
			setIsBrokerConfirm(true);
		}
	}, [moneyConfirmPaymentId]);

	useEffect(() => {
		if (handshakeRejectPaymentId !== '' && handshakeRejectPaymentId === currentPaymentId) {
			setIsHandshakeReject(true);
		}
	}, [isBrokerConfirm]);

	useEffect(() => {
		const handShakeConfirmEmitter = LoanPaymentContract.events.paymentHandshakeConfirm({ fromBlock: 'latest' }, (error, response) => {
			const result = response.returnValues;

			setHandshakeConfirmPaymentId(result.id);
		});

		const paymentCancelEmitter = LoanPaymentContract.events.paymentCancel({ fromBlock: 'latest' }, (error, response) => {
			const result = response.returnValues;

			setHandshakeRejectPaymentId(result.id);
		});

		const flagPaymentEmitter = LoanPaymentContract.events.flagPayment({ fromBlock: 'latest' }, (error, response) => {
			const result = response.returnValues;

			if (result.reason.includes('Broker')) {
				setIsBrokerReject(true);
			}
		});

		const moneyConfirmEmitter = LoanPaymentContract.events.moneyReceived({ fromBlock: 'latest' }, (error, response) => {
			const result = response.returnValues;
			setMoneyConfirmPaymentId(result.id);
		});

		return () => {
			handShakeConfirmEmitter.unsubscribe();
			paymentCancelEmitter.unsubscribe();
			flagPaymentEmitter.unsubscribe();
			moneyConfirmEmitter.unsubscribe();
			console.log('Cleanup functions');
		};
	}, []);

	const steps = [
		{
			title: 'Start Handshake',
		},
		{
			title: 'Handshake Confirmation',
		},
		{
			title: 'Handover Money',
		},
		{
			title: 'Money Confirmation',
		},
	];

	const customDot = (dot, { status, index }) => (
		<Popover
			content={
				<span>
					step {index} status: {status}
				</span>
			}
		>
			{dot}
		</Popover>
	);

	const setStateValues = (pinNumber) => {
		setPin(pinNumber);
	};

	return (
		<Card title="Handover Money">
			<Title level={4}>Token convertion rate: S${TOKEN_VALUE}</Title>
			<Divider />

			<Row>
				<Col lg={24} xl={18} xxl={16} style={{ marginBottom: 25 }}>
					<Steps current={current} progressDot={customDot}>
						{steps.map(item => (
							<Step key={item.title} title={item.title} icon={item.icon} />
						))}
					</Steps>
				</Col>
			</Row>
			{
				current === 0 &&
				<Row>
					<Col lg={24} xl={18} xxl={16}>
						<MoneyHandoverForm
							setBroker={setBroker}
							setLoanId={setLoanId}
							setAmount={setAmount}
							setTokens={setTokens}
							setPin={setPin}
							setStatesValues={setStateValues}
							tokenValue={TOKEN_VALUE}
							startHandshake={startHandshake}
						/>
					</Col>
				</Row>
			}
			{
				current === 1 && !isHandshakeConfirm && !isHandshakeReject &&
				<Row>
					<Col lg={24} xl={18} xxl={16}>
						<HandshakeWaitingForm />
					</Col>
				</Row>
			}
			{
				current === 1 && isHandshakeConfirm &&
				<Row>
					<Col lg={24} xl={18} xxl={16}>
						<HandshakeSuccess
							cancelByBorrower={cancelByBorrower}
							next={next}
						/>
					</Col>
				</Row>
			}
			{
				current === 1 && isHandshakeReject &&
				<Row>
					<Col lg={24} xl={18} xxl={16}>
						<HandshakeFail backToHome={backToHome} />
					</Col>
				</Row>
			}
			{
				current === 2 &&
				// current === 0 &&
				<Row>
					<Col lg={24} xl={18} xxl={16}>
						<MoneyHandoverConfirm
							broker={broker}
							amount={amount}
							paymentId={currentPaymentId}
							loanId={loanId}
							tokens={tokens}
							handoverMoney={handoverMoney}
							cancelByBorrower={cancelByBorrower}
						/>
					</Col>
				</Row>
			}
			{
				current === 3 && !isTimesUp && !isBrokerConfirm && !isBrokerReject &&
				<Row>
					<Col lg={24} xl={18} xxl={16}>
						<BrokerConfirmationWaiting
							setIsTimesUp={setIsTimesUp}
						/>
					</Col>
				</Row>
			}
			{
				current === 3 && isTimesUp && !isBrokerConfirm && !isBrokerReject &&
				<Row>
					<Col lg={24} xl={18} xxl={16}>
						<CancelByBorrower
							flagByBorrower={flagByBorrower}
						/>
					</Col>
				</Row>
			}
			{
				current === 3 && isBrokerConfirm &&
				<Row>
					<Col lg={24} xl={18} xxl={16}>
						<BrokerConfirmationSuccess
							backToHome={backToHome}
						/>
					</Col>
				</Row>
			}
			{
				current === 3 && isBrokerReject &&
				<Row>
					<Col lg={24} xl={18} xxl={16}>
						<BrokerConfirmationFail
							backToHome={backToHome}
						/>
					</Col>
				</Row>
			}
		</Card>

	);
}

export default MoneyHandoverBorrowerController;
