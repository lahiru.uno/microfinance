import React from 'react';
import { Result, Button } from 'antd';

function BrokerConfirmationSuccess({ backToHome }) {
	return (
		<Result
			status="success"
			title="Broker confirm the money!"
			subTitle="Money handover successfull."
			extra={[
				<Button type="primary" key="next" onClick={(e) => backToHome(e)}>
					Back to home
				</Button>,
			]}
		/>

	);
}

export default BrokerConfirmationSuccess;
