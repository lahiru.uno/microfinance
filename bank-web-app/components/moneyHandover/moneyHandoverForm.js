import React, { useState } from 'react';
import { Form, Input, Button, InputNumber } from 'antd';

function MoneyHandoverForm({
	setBroker,
	setLoanId,
	setAmount,
	setTokens,
	tokenValue,
	setStatesValues,
	setPin,
}) {
	const [componentSize] = useState('default');
	const [form] = Form.useForm();

	const onFinish = (values) => {
		setBroker(values.broker);
		setLoanId(values.loanId);
		setAmount(values.amount);
		setTokens(values.tokens);
		setStatesValues(values.pin);
		setPin(values.pin);
	};

	const changeAmount = (e) => {
		// .target.value
		console.log(e);
		form.setFieldsValue({
			amount: e * tokenValue,
		});
	};

	return (
		<Form
			form={form}
			labelCol={{
				lg: 5,
				xl: 4,
				xxl: 3,
			}}
			wrapperCol={{
				lg: 14,
				xl: 12,
				xxl: 10,
			}}
			layout="horizontal"
			initialValues={{
				size: componentSize,
			}}
			size={componentSize}
			labelAlign="left"
			onFinish={onFinish}
		>
			<Form.Item label="Broker" name="broker" rules={[{ required: true, message: 'Please enter broker!' }]}>
				<Input
					placeholder="Enter broker address"
				/>
			</Form.Item>
			<Form.Item label="Loan ID" name="loanId" rules={[{ required: true, message: 'Please enter Loan ID!' }]}>
				<InputNumber
					min="0"
					style={{ width: '100%' }}
					placeholder="Enter loan id"
				/>
			</Form.Item>
			<Form.Item label="Token Amount" name="tokens" rules={[{ required: true, message: 'Please enter Token Amount!' }]}>
				<InputNumber
					min="0"
					style={{ width: '100%' }}
					placeholder="Enter token amount"
					onChange={(e) => changeAmount(e)}
				/>
			</Form.Item>
			<Form.Item label="Amount" name="amount" rules={[{ required: true, message: 'Please enter amount!' }]}>
				<InputNumber
					min="0"
					style={{ width: '100%' }}
					placeholder="Enter amount"
					disabled="True"
				/>
			</Form.Item>
			<Form.Item label="PIN" name="pin" rules={[{ required: true, message: 'Please enter pin!' }]}>
				<InputNumber
					min="0"
					max="9999"
					style={{ width: '100%' }}
					placeholder="Enter 4 digit PIN number"
				/>
			</Form.Item>
			<Form.Item wrapperCol={{
				lg: { span: 14, offset: 5 },
				xl: { span: 14, offset: 4 },
				xxl: { span: 14, offset: 3 } }}
			>
				<Button type="primary" htmlType="submit">Start handshake</Button>
			</Form.Item>
		</Form>

	);
}

export default MoneyHandoverForm;
