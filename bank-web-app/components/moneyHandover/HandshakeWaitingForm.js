import React from 'react';
import { Spin, Alert } from 'antd';

function HandshakeWaitingForm() {
	return (
		<>
			<Spin tip="Waiting..." size="large">
				<Alert
					message="Waiting for handshake"
					description="Wait for broker accepts the handshake"
					type="info"
				/>
			</Spin>
		</>

	);
}

export default HandshakeWaitingForm;
