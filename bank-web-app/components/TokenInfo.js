import React, { useEffect, useState, useContext } from 'react';
import { Card, Table, message } from 'antd';
// import InputDataDecoder from 'ethereum-input-data-decoder';
import AuthContext from '../stores/authContext';

function TokenInfo() {
	const [totalSupply, setTotalSupply] = useState('0.00');
	const [decimals, setDecimals] = useState('0.00');
	const { MicroTokenContract, BankLoanContractAbi } = useContext(AuthContext);

	const getTotalSupply = async () => {
		try {
			const response = await MicroTokenContract.methods.totalSupply().call();
			setTotalSupply(response);
		} catch (err) {
			message.error('Error occured while reading totalSupply');
		}
	};

	const getDecimals = async () => {
		try {
			const response = await MicroTokenContract.methods.decimals().call();
			setDecimals(response);
		} catch (err) {
			message.error('Error occured while reading decimals');
		}
	};

	// const decodeInput = () => {
	// 	const decoder = new InputDataDecoder(BankLoanContractAbi);
	// 	const data = '0x9daafc6c00000000000000000000000000000000000000000000000000000000000003e8000000000000000000000000000000000000000000000000000000000000000c000000000000000000000000000000000000000000000000000000000000000a00000000000000000000000000000000000000000000000000000000000000a0000000000000000000000000d24a27ba895e41ce8384fa80dd63fdf1a8a381d600000000000000000000000000000000000000000000000000000000000000013100000000000000000000000000000000000000000000000000000000000000';

	// 	const result = decoder.decodeData(data);

	// 	console.log(result);
	// };

	useEffect(() => {
		getTotalSupply();
		getDecimals();
		// decodeInput();
	}, []);

	const columns = [
		{ title: 'Attribute', dataIndex: 'attribute', key: 'attribute', width: '20%' },
		{ title: 'Description', dataIndex: 'description', key: 'description' },
	];

	const data = [
		{
			attribute: 'Contract address',
			description: MicroTokenContract._address,
		},
		{
			attribute: 'Total supply',
			description: totalSupply,
		},
		{
			attribute: 'Decimals',
			description: decimals,
		},
	];

	return (
		<Card title="Micro Tokens informations">
			<Table columns={columns} dataSource={data} pagination={false} size="small" columnWidth="30%" />
		</Card>
	);
}

export default TokenInfo;
