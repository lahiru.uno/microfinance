import React from 'react';
import 'antd/dist/antd.css';
import { Menu } from 'antd';

import { useRouter } from 'next/router';

function BrokerMenu() {
	const router = useRouter();
	const { SubMenu } = Menu;

	return (
		<Menu
			mode="inline"
			defaultSelectedKeys={['/transfer']}
			// defaultOpenKeys={['sub1']}
			style={{ height: '100%', borderRight: 0 }}
		>
			<Menu.Item key="/transfer" onClick={() => router.push('/broker/transfer')}>
				Transfer
			</Menu.Item>
			{/* <Menu.Item key="/deals" onClick={() => router.push('/deals')}>
						Deals
			</Menu.Item> */}
			{/* <Menu.Item key="/loans1" onClick={() => router.push('/broker/loans')}>
				Loans
			</Menu.Item> */}
			<SubMenu key="loans" title="Loans">
				<Menu.Item key="/view-loans" onClick={() => router.push('/broker/view-loans')}>Current Loans</Menu.Item>
				<Menu.Item key="/apply-loans" onClick={() => router.push('/broker/apply-loans')}>Apply Loan</Menu.Item>
			</SubMenu>
			<Menu.Item key="/receive-money" onClick={() => router.push('/broker/receive-money')}>
				Receive Money
			</Menu.Item>
			<Menu.Item key="/register-borrower" onClick={() => router.push('/broker/register-borrower')}>
				Register Borrower
			</Menu.Item>
			<Menu.Item key="/blockchain" onClick={() => router.push('/public/blockchain')}>
				Blockchain
			</Menu.Item>
			<Menu.Item key="/info" onClick={() => router.push('/public/info')}>
				Info
			</Menu.Item>
		</Menu>
	);
}

export default BrokerMenu;
