import React from 'react';
import { Row, Col } from 'antd';
import MoneyHandoverBorrowerController from '../../components/moneyHandover/moneyHandoverController';

function Transfer() {
	return (
		<>
			<Row gutter={[16, 16]}>
				<Col span={24}>
					<MoneyHandoverBorrowerController />
				</Col>
			</Row>
		</>
	);
}

export default Transfer;
