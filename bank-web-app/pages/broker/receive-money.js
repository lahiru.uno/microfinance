import React from 'react';
import { Row, Col } from 'antd';
import ReceiveMoneyTable from '../../components/moneyHandover/ReceiveMoneyTable';

function ReceiveMoney() {
	return (
		<Row gutter={[16, 16]}>
			<Col span={24}>
				<ReceiveMoneyTable />
			</Col>
		</Row>
	);
}

export default ReceiveMoney;
