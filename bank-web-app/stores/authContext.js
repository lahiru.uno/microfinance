import React, { createContext, useState } from 'react';

import Web3 from 'web3';
import IPFS from 'ipfs-api';
import MicroTokenArtifact from '../../blockchain/build/contracts/MicroToken.json';
import BankLoanArtifact from '../../blockchain/build/contracts/BankLoan.json';
import UserIdentityArtifact from '../../blockchain/build/contracts/UserIdentity.json';
import LoanPaymentArtifact from '../../blockchain/build/contracts/LoanPayment.json';

const AuthContext = createContext({
	user: null,
	userRole: null,
	login: () => {},
});

export const AuthContextProvider = ({ children }) => {
	const users = [
		{
			name: 'Leonard Hofstadter',
			role: 'broker',
			color: '#87d068',
		},
		{
			name: 'Sheldon Cooper',
			role: 'bank',
			color: '#8193E7',
		},
		{
			name: 'Rajesh Koothrapali',
			role: 'borrower',
			color: '#8193E7',
		},
		{
			name: 'Guest',
			role: 'public',
			color: '#8193E7',
		},
	];

	const [user, setUser] = useState(users[0]);

	const login = (role) => {
		if (role === 'broker') {
			setUser(users[0]);
		} else if (role === 'bank') {
			setUser(users[1]);
		} else if (role === 'borrower') {
			setUser(users[2]);
		} else if (role === 'public') {
			setUser(users[3]);
		}
	};
	const web3 = new Web3(Web3.givenProvider || 'http://127.0.0.1:7545');

	// Smart Contract Addresses
	const MicroTokenAddress = MicroTokenArtifact.networks[5777].address;
	const UserIdentityAddress = UserIdentityArtifact.networks[5777].address;
	const BankLoanAddress = BankLoanArtifact.networks[5777].address;
	const LoanPaymentAddress = LoanPaymentArtifact.networks[5777].address;

	const BankLoanContractAbi = BankLoanArtifact.abi;

	const UserIdentityContract = new web3.eth.Contract(UserIdentityArtifact.abi, UserIdentityAddress);
	const MicroTokenContract = new web3.eth.Contract(MicroTokenArtifact.abi, MicroTokenAddress);
	const BankLoanContract = new web3.eth.Contract(BankLoanArtifact.abi, BankLoanAddress);
	const LoanPaymentContract = new web3.eth.Contract(LoanPaymentArtifact.abi, LoanPaymentAddress);

	const ipfs = new IPFS({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' });

	const context = { user,
		login,
		web3,
		ipfs,
		MicroTokenContract,
		UserIdentityContract,
		BankLoanContract,
		LoanPaymentContract,
		BankLoanContractAbi,
		MicroTokenAddress,
		UserIdentityAddress,
		BankLoanAddress,
		LoanPaymentAddress,
	};

	return (
		<AuthContext.Provider value={context}>
			{children}
		</AuthContext.Provider>
	);
};

export default AuthContext;
