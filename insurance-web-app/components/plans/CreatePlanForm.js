import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Form, InputNumber, Button, message } from 'antd';
import { postApi } from '../../util/fetchApi';

function CreatePlanForm({ togglePlan, setTogglePlan }) {
	const [componentSize, setComponentSize] = useState('default');

	const onFormLayoutChange = ({ size }) => {
		setComponentSize(size);
	};

	const createPolicyPlan = async (values) => {
		try {
			const body = {
				months: values.duration,
				loanAmount: values.amount,
				initialPayment: values.initial,
				finalPayment: values.final,
			};

			const requestOptions = {
				// method: 'POST',
				body: JSON.stringify(body),
			};

			const response = await postApi({
				url: 'policy-plans',
				options: requestOptions,
			});

			const result = await response;
			await console.log(result);

			message.success('New Insurance Polciy Plan added successfully');
			setTogglePlan(!togglePlan);
		} catch (err) {
			message.error('Error creating Insurance Polciy Plan');
			console.log(err);
		}
	};

	return (
		<Card title="Create Insurance Policy Plan" style={{ margin: '0px' }}>
			<Form
				labelCol={{
					lg: 4,
					xl: 3,
					xxl: 2,
				}}
				wrapperCol={{
					lg: 14,
					xl: 12,
					xxl: 10,
				}}
				layout="horizontal"
				initialValues={{
					size: componentSize,
				}}
				onValuesChange={onFormLayoutChange}
				size={componentSize}
				labelAlign="left"
				onFinish={createPolicyPlan}
			>
				<Form.Item label="Loan Amount" name="amount" rules={[{ required: true, message: 'Please enter Loan Amount!' }]}>
					<InputNumber
						min="0"
						style={{ width: '100%' }}
						placeholder="Enter Loan Amount"
					/>
				</Form.Item>
				<Form.Item label="Loan Duration" name="duration" rules={[{ required: true, message: 'Please enter Loan Duration!' }]}>
					<InputNumber
						min="0"
						style={{ width: '100%' }}
						placeholder="Enter Loan Duration"
					/>
				</Form.Item>
				<Form.Item label="Initial Payment" name="initial" rules={[{ required: true, message: 'Please enter Initial Insurance Payment!' }]}>
					<InputNumber
						min="0"
						style={{ width: '100%' }}
						placeholder="Enter Initial Insurance Payment"
					/>
				</Form.Item>
				<Form.Item label="Final Payment" name="final" rules={[{ required: true, message: 'Please enter Final Insurance Payment!' }]}>
					<InputNumber
						min="0"
						style={{ width: '100%' }}
						placeholder="Enter Final Insurance Payment"
					/>
				</Form.Item>
				<Form.Item wrapperCol={{
					lg: { span: 14, offset: 4 },
					xl: { span: 14, offset: 3 },
					xxl: { span: 14, offset: 2 } }}
				>
					<Button type="primary" htmlType="submit">Add New Plan</Button>
				</Form.Item>
			</Form>
		</Card>
	);
}

CreatePlanForm.propTypes = {
	togglePlan: PropTypes.bool.isRequired,
	setTogglePlan: PropTypes.func.isRequired,
};

export default CreatePlanForm;
