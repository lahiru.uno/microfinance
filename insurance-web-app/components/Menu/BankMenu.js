import React from 'react';
import 'antd/dist/antd.css';
import { Menu } from 'antd';

import { useRouter } from 'next/router';

function BrokerMenu() {
	const router = useRouter();

	return (
		<Menu
			mode="inline"
			defaultSelectedKeys={['/policies']}
			style={{ height: '100%', borderRight: 0 }}
		>
			<Menu.Item key="/policies" onClick={() => router.push('/broker/policies')}>
				Policies
			</Menu.Item>
			<Menu.Item key="/transfer" onClick={() => router.push('/public/transfer')}>
				Transfer
			</Menu.Item>
			<Menu.Item key="/blockchain" onClick={() => router.push('/public/blockchain')}>
				Blockchain
			</Menu.Item>
			<Menu.Item key="/info" onClick={() => router.push('/public/info')}>
				Info
			</Menu.Item>
		</Menu>
	);
}

export default BrokerMenu;
