import React, { useState } from 'react'
import { Typography, Card,
    Form,
    Input,
    Button,
    Radio,
    Select,
    Cascader,
    DatePicker,
    InputNumber,
    TreeSelect,
    Switch
    } from 'antd'; 

const {Title} = Typography

function TransferForm() {
    const [componentSize, setComponentSize] = useState('default');

    return (
        <Card title="Transfer ERC-20 Tokens" style={{ margin: '0px 12px 12px 0px' }}>
            <Form
                labelCol={{
                    span: 4,
                }}
                wrapperCol={{
                    span: 18,
                }}
                layout="horizontal"
                initialValues={{
                    size: componentSize,
                }}
                size={componentSize}
            >
                <Form.Item label="Receiver">
                    <Input placeholder="Enter receiver address"/>
                </Form.Item>
                <Form.Item label="Amount">
                    <InputNumber min="0" style={{ width: '100%' }} placeholder="Enter amount"/>
                </Form.Item>
                <Form.Item wrapperCol={{ span: 14, offset: 4 }}>
                    <Button type="primary">Transfer tokens</Button>
                </Form.Item>
            </Form>
        </Card>

    );
}

export default TransferForm;