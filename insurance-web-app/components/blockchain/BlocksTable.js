import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, message } from 'antd';
import Moment from 'react-moment';
import AuthContext from '../../stores/authContext';

function BlocksTable() {
	const { web3 } = useContext(AuthContext);

	const columns = [
		{
			title: 'Block ID',
			dataIndex: 'id',
			key: 'id',
			sortOrder: 'descend',
			sorter: (a, b) => a.id - b.id,
			width: '10%',
			// render: text => text,
		},
		{
			title: 'Hash',
			dataIndex: 'hash',
			key: 'hash',
			width: '50%',
			ellipsis: true,
		},
		{
			title: 'Time',
			dataIndex: 'time',
			key: 'time',
			width: '20%',
			render: time => (
				<Moment unix>{time}</Moment>
			),
		},
		{
			title: 'Number of Transactions',
			dataIndex: 'transactionCount',
			key: 'transactionCount',
			width: '20%',
			render: count => (
				<>
					{count === 0 && <Tag color="red"> No Transactions </Tag>}
					{count === 1 && <Tag color="blue"> {count} Transaction </Tag>}
					{count > 1 && <Tag color="blue"> {count} Transactions </Tag>}
				</>
			),
		},
	];

	const [data, setData] = useState([]);

	const addData = async (error, block) => {
		const transactionData = [];

		if (block.transactions.length > 0) {
			for (let i = 0; i < block.transactions.length; i++) {
				const response = await web3.eth.getTransaction(block.transactions[i])
				console.log(response);
				const reciept = await web3.eth.getTransactionReceipt(block.transactions[i])
				console.log(reciept);
				const t = {
					hash: response.hash,
					from: response.from,
					to: response.to,
					status: reciept.status,
					gasUsed: reciept.gasUsed,
					contractAddress: reciept.contractAddress,
					value: response.value,
					gasPrice: response.gasPrice,
					gas: response.gas,
					nonce: response.nonce,
					cumulativeGasUsed: reciept.cumulativeGasUsed,
					input: response.input,
				};
				transactionData.push(t);
			}
		}

		const row = {
			key: block.number,
			id: block.number,
			hash: block.hash,
			time: block.timestamp,
			transactions: block.transactions,
			transactionCount: block.transactions.length,
			transactionData,
		};

		setData((prev) => {
			return [...prev, row];
		});

		console.log(data);
	};

	const getBlocks = async () => {
		try {
			setData([]);

			const latest = await web3.eth.getBlockNumber();

			const batch = new web3.eth.BatchRequest();

			const limit = latest > 20 ? 20 : latest;

			for (let i = 0; i <= limit; i++) {
				batch.add(web3.eth.getBlock.request(latest - i, addData));
			}

			batch.execute();
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading blocks');
		}
	};

	useEffect(() => {
		getBlocks();
	}, []);

	function expandedTransactionsRow(record) {
		const transactionInfoColumns = [
			{ title: 'Attribute', dataIndex: 'attribute', key: 'attribute', width: '20%' },
			{ title: 'Value', dataIndex: 'value', key: 'value', width: '80%', ellipsis: true },
			// { title: 'Description', dataIndex: 'description', key: 'description' }
		];

		const transactionData = [
			{
				attribute: 'From',
				value: record.transactionData.from,
				description: 'Address of the sender.',
			},
			{
				attribute: 'To',
				value: record.transactionData.to,
				description: 'Address of the receiver. Null when its a contract creation transaction.',
			},
			{
				attribute: 'Status',
				value: record.transactionData.status ?
					<Tag color="green"> TRUE </Tag> :
					<Tag color="red"> FALSE </Tag>,
				description: 'TRUE if the transaction was successful, FALSE, if the EVM reverted the transaction.',
			},
			{
				attribute: 'Input',
				value: record.transactionData.input,
			},
			{
				attribute: 'Contract address',
				value: record.transactionData.contractAddress,
				description: 'The contract address created, if the transaction was a contract creation, otherwise Null',
			},
			{
				attribute: 'Value',
				value: record.transactionData.value,
				description: 'Value transferred in wei.',
			},
			{
				attribute: 'Gas',
				value: record.transactionData.gas,
				description: 'Gas provided by the sender.',
			},
			{
				attribute: 'Gas price',
				value: record.transactionData.gasPrice,
				description: 'Gas price provided by the sender in wei.',
			},
			{
				attribute: 'Gas used',
				value: record.transactionData.gasUsed,
				description: 'The amount of gas used by this specific transaction alone.',
			},
			{
				attribute: 'Cumulative gas used',
				value: record.transactionData.cumulativeGasUsed,
				description: 'The total amount of gas used when this transaction was executed in the block.',
			},
			{
				attribute: 'Nonce',
				value: record.transactionData.nonce,
				description: 'The number of transactions made by the sender prior to this one.',
			},
		];

		return <Table columns={transactionInfoColumns} dataSource={transactionData} pagination={false} size="small" />;
	}

	const expandedRowRender = (record) => {
		console.log(record);

		const expandedData = [];

		for (let i = 0; i < record.transactions.length; i++) {
			const t = {
				id: i + 1,
				transaction: record.transactions[i],
				transactionData: record.transactionData[i],
			};

			expandedData.push(t);
			// console.log(t)
		}
		// console.log(data)

		const expandedColumns = [
			{ title: 'Transactions', dataIndex: 'transaction', key: 'transaction' },
		];

		return (
			<Table
				columns={expandedColumns}
				dataSource={expandedData}
				pagination={false}
				expandable={{ expandedRowRender: expandedTransactionsRow }}
			/>);
	};

	return (
		<>
			<Card title="Blockchain" extra={<a href="javascript:void(0);" onClick={() => getBlocks()}>Refresh</a>}>
				<Table
					columns={columns}
					dataSource={data}
					expandable={{
						expandedRowRender,
						rowExpandable: record => record.transactions.length > 0,
					}}
				/>
			</Card>
		</>
	);
}

export default BlocksTable;
