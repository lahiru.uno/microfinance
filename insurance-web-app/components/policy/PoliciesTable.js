import React, { useState, useContext, useEffect } from 'react';

import { Table, Tag, Card, Divider, message, Modal, Form } from 'antd';
import AuthContext from '../../stores/authContext';
import { getApi } from '../../util/fetchApi';

function PoliciesTable() {
	const { user, InsurancePolicy, UserIdentity } = useContext(AuthContext);

	const state = ['REQUESTED', 'BORROWER_SIGNED', 'APPROVED', 'REJECTED', 'ONGOING', 'BORROWER_PAID', 'CLAIM_REQUESTED', 
		'CLAIM_APPROVED', 'CLAIM_REJECTED', 'CLAIMED', 'CLOSE'];

	const [payments, setPayments] = useState([]);
	const [componentSize, setComponentSize] = useState('default');

	const [editId, setEditId] = useState('');
	const [isModalVisible, setIsModalVisible] = useState('');
	const [data, setData] = useState([]);

	const brokers = {};
	const borrowers = {};

	const getPayments = async () => {
		try {
			const response = await getApi({
				url: 'policy-payments',
			});
			const paymentsResult = await response;
			console.log(paymentsResult);
			setPayments(paymentsResult);
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading Insurance Policy Payments');
		}
	};

	const getAllBorrowers = async () => {
		const response = await UserIdentity.methods.getAllBorrowers().call();
		for (let i = 0; i < response.length; i++) {
			borrowers[response[i].userAddress] = response[i].name;
		}
	};

	const getBrokers = async () => {
		const response = await UserIdentity.methods.getAllBrokers().call();
		for (let i = 0; i < response.length; i++) {
			brokers[String(response[i].userAddress)] = response[i].name;
		}
		console.log(brokers);
	};

	const getPolicies = async () => {
		try {
			const response = await InsurancePolicy.methods.getAllPolicies().call();

			setData([]);

			for (let i = 0; i < response.length; i++) {
				const row = {
					key: response[i].id,
					id: response[i].id,
					amount: response[i].amount,
					period: response[i].months,
					planId: response[i].planId,
					loanId: response[i].loanId,
					cover: response[i].cover,
					borrower: borrowers[response[i].borrower],
					broker: brokers[response[i].broker],
					brokerAddress: response[i].broker,
					borrowerAddress: response[i].borrower,
					status: response[i].state,
					borrowerSigned: response[i].isBorrowerSigned,
					insuranceApprove: response[i].insuranceApprove,
				};

				setData((prev) => {
					return [...prev, row];
				});
			}
		} catch (err) {
			message.error('Error occured while loading plans');
		}
	};

	const signPolicyRequest = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await InsurancePolicy.methods.signInsurancePolicy(editId).send({ from: accounts[0] });
			message.success(`Insurance Policy ${editId} signed`);
			getPolicies();
		} catch (err) {
			console.log(err);
			message.error('Error occured while signing Insurance Policy');
		}
	};

	const approveInsurancePolicy = async (policyId) => {
		try {
			const accounts = await window.ethereum.enable();
			await InsurancePolicy.methods.approveInsurancePolicy(policyId).send({ from: accounts[0] });
			message.success(`Insurance Policy ${policyId} approved`);
			getPolicies();
		} catch (err) {
			message.error('Error occured while approving the Insurance Policy');
		}
	};

	const rejectInsurancePolicy = async (policyId) => {
		try {
			const accounts = await window.ethereum.enable();
			await InsurancePolicy.methods.rejectInsurancePolicy(policyId).send({ from: accounts[0] });
			message.success(`Insurance Policy ${policyId} rejected`);
			getPolicies();
		} catch (err) {
			message.error('Error occured while approving the Insurance Policy');
		}
	};

	const confirmPaymentFromBank = async (policyId) => {
		try {
			const accounts = await window.ethereum.enable();
			await InsurancePolicy.methods.confirmPaymentFromBank(policyId).send({ from: accounts[0] });
			message.success(`Insurance Policy ${policyId} updated`);
			getPolicies();
		} catch (err) {
			message.error('Error occured while updating the Insurance Policy');
		}
	};

	const confirmPaymentFromBorrower = async (policyId) => {
		try {
			const accounts = await window.ethereum.enable();
			await InsurancePolicy.methods.confirmPaymentFromBorrower(policyId).send({ from: accounts[0] });
			message.success(`Insurance Policy ${policyId} updated`);
			getPolicies();
		} catch (err) {
			message.error('Error occured while updating the Insurance Policy');
		}
	};

	const closePolicy = async (policyId) => {
		try {
			const accounts = await window.ethereum.enable();
			await InsurancePolicy.methods.closePolicy(policyId).send({ from: accounts[0] });
			message.success(`Insurance Policy ${policyId} updated`);
			getPolicies();
		} catch (err) {
			message.error('Error occured while updating the Insurance Policy');
		}
	};

	const requestClaim = async (policyId) => {
		try {
			const accounts = await window.ethereum.enable();
			await InsurancePolicy.methods.requestClaim(policyId).send({ from: accounts[0] });
			message.success('Claim requested successfully');
			getPolicies();
		} catch (err) {
			message.error('Error occured while requesting claim');
		}
	};

	const approveClaim = async (policyId) => {
		try {
			const accounts = await window.ethereum.enable();
			await InsurancePolicy.methods.approveClaim(policyId).send({ from: accounts[0] });
			message.success('Claim approved');
			getPolicies();
		} catch (err) {
			message.error('Error occured while approving claim');
		}
	};

	const rejectClaim = async (policyId) => {
		try {
			const accounts = await window.ethereum.enable();
			await InsurancePolicy.methods.rejectClaim(policyId).send({ from: accounts[0] });
			message.success('Claim rejected');
			getPolicies();
		} catch (err) {
			message.error('Error occured while rejecting claim');
		}
	};
	const confirmClaim = async (policyId) => {
		try {
			const accounts = await window.ethereum.enable();
			await InsurancePolicy.methods.confirmClaim(policyId).send({ from: accounts[0] });
			message.success(`Insurance Policy ${policyId} updated`);
			getPolicies();
		} catch (err) {
			message.error('Error occured while updating the Insurance Policy');
		}
	};

	const showModal = (value) => {
		setEditId(value);
		setIsModalVisible(true);
	};

	const handleOk = () => {
		signPolicyRequest();
		setIsModalVisible(false);
	};

	const handleCancel = () => {
		setIsModalVisible(false);
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
			render: text => text,
		},
		{
			title: 'Borrower',
			dataIndex: 'borrower',
			key: 'borrower',
		},
		{
			title: 'Broker',
			dataIndex: 'broker',
			key: 'broker',
		},
		{
			title: 'Amount',
			dataIndex: 'amount',
			key: 'amount',
		},
		{
			title: 'Period',
			dataIndex: 'period',
			key: 'period',
		},
		{
			title: 'Loan ID',
			key: 'loanId',
			dataIndex: 'loanId',
		},
		{
			title: 'Plan ID',
			key: 'planId',
			dataIndex: 'planId',
		},
		{
			title: 'Insurance Fee',
			key: 'cover',
			dataIndex: 'cover',
		},
		{
			title: 'Status',
			key: 'status',
			dataIndex: 'status',
			render: tag => {
				let color = 'geekblue';
				if (tag === '3' || tag === '8') {
					color = 'red';
				} else if (tag === '2' || tag === '7' || tag === '9') {
					color = 'green';
				}
				return (
					<Tag color={color} key={tag}>
						{state[tag]}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'borrower') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: '',
			render: (record) => {
				if (record.status === '0') {
					return (
						<span>
							<a href="javascript:void(0);" onClick={() => showModal(record.id)}>Sign</a>
						</span>
					);
				}
			},
		});
	}

	if (user.role === 'bank') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: '',
			render: (record) => {
				let actionBlock = '';
				if (record.status === '4') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => requestClaim(record.id)}>Request Claim</a>
						</span>;
				} else if (record.status === '7') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => confirmClaim(record.id)}>Claim Received</a>
						</span>;
				}
				return actionBlock;
			},
		});
	}

	if (user.role === 'insurance') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: '',
			render: (record) => {
				let actionBlock = '';
				if (record.status === '1') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => approveInsurancePolicy(record.id)}>Approve</a>
							<Divider type="vertical" />
							<a href="javascript:;" style={{ color: 'red' }} onClick={() => rejectInsurancePolicy(record.id)}>Reject</a>
						</span>;
				} else if (record.status === '2') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => confirmPaymentFromBank(record.id)}>Token Received</a>
						</span>;
				} else if (record.status === '4') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => confirmPaymentFromBorrower(record.id)}>Borrower Paid</a>
						</span>;
				} else if (record.status === '5') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => closePolicy(record.id)}>Close Insurance Policy</a>
						</span>;
				} else if (record.status === '6') {
					actionBlock =
						<span>
							<a href="javascript:void(0);" onClick={() => approveClaim(record.id)}>Approve Claim</a>
							<Divider type="vertical" />
							<a href="javascript:;" style={{ color: 'red' }} onClick={() => rejectClaim(record.id)}>Reject Claim</a>
						</span>;
				}
				return actionBlock;
			},
		});
	}

	const loadData = async () => {
		await getAllBorrowers();
		await getBrokers();
		await getPayments();
		await getPolicies();
	};

	useEffect(() => {
		loadData();
		const emitter = InsurancePolicy.events.insurancePolicyRequest({ fromBlock: 'latest' }, (error, response) => {
			const result = response.returnValues;

			const row = {
				key: result.id,
				id: result.id,
				amount: result.amount,
				period: result.months,
				planId: result.planId,
				loanId: result.loanId,
				cover: result.cover,
				borrower: borrowers[result.borrower],
				broker: brokers[result.broker],
				brokerAddress: result.broker,
				borrowerAddress: result.borrower,
				status: result.state,
				borrowerSigned: result.isBorrowerSigned,
				insuranceApprove: result.insuranceApprove,
			};

			setData((prev) => {
				return [...prev, row];
			});
		});

		return () => {
			emitter.unsubscribe();
		};
	}, []);

	// const expandedRowRender = (record) => {
	// 	const expandedData = [];

	// 	expandedData.push(record);

	// 	const expandedColumns = [
	// 		{ title: 'Borrower Address', dataIndex: 'borrowerAddress', key: 'borrowerAddress', width: '30%' },
	// 		{ title: 'Broker Address', dataIndex: 'brokerAddress', key: 'brokerAddress', width: '30%' },
	// 	];

	// 	return (
	// 		<>
	// 			<Table
	// 				columns={expandedColumns}
	// 				dataSource={expandedData}
	// 				pagination={false}
	// 			/>
	// 		</>
	// 	);
	// };

	const expandedRowRender = (record) => {
		const expandedData = [];

		expandedData.push(record);

		const expandedPayments = payments.filter(item => item.policyId == record.id);

		const expandedPaymentColumns = [
			{ title: 'Payment ID', dataIndex: '_id', key: 'id' },
			{ title: 'Amount', dataIndex: 'amount', key: 'amount' },
			{ title: 'Insurance Policy ID', dataIndex: 'policyId', key: 'policyId' },
			{ title: 'Transaction Hash', dataIndex: 'transactionHash', key: 'transactionHash' },
		];

		return (
			<>
				<Form
					labelCol={{
						lg: 6,
						xl: 6,
						xxl: 3,
					}}
					wrapperCol={{
						lg: 12,
						xl: 12,
						xxl: 16,
					}}
					layout="horizontal"
					initialValues={{
						size: componentSize,
					}}
					size={componentSize}
					labelAlign="left"
				>
					<Form.Item label="Borrower address">
						<span>{record.borrower}</span>
					</Form.Item>
					<Form.Item label="Broker address">
						<span>{record.broker}</span>
					</Form.Item>
				</Form>
				<Table
					columns={expandedPaymentColumns}
					dataSource={expandedPayments}
					pagination={false}
				/>
			</>
		);
	};

	return (
		<>
			<Card title="Current Policies" extra={<a href="javascript:void(0);" onClick={() => loadData()}>Refresh</a>}>
				<Table
					pagination="true"
					columns={columns}
					dataSource={data}
					expandable={{
						expandedRowRender,
					}}
				/>
			</Card>
			<Modal title={`Sign Insurance Policy  ${editId}`} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
				<p>Are you sure you want to sign this policy request?</p>
			</Modal>
		</>
	);
}

export default PoliciesTable;
