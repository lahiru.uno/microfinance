import React, { useState } from 'react';
import { Form, Input, Button, InputNumber} from 'antd';

function TransferForm({ address, amount, setAddress, setAmount, transferTokens }) {
	const [componentSize, setComponentSize] = useState('default');

	return (
		<Form
			labelCol={{
				lg: 3,
				xl: 2,
				xxl: 2,
			}}
			wrapperCol={{
				lg: 14,
				xl: 12,
				xxl: 10,
			}}
			layout="horizontal"
			initialValues={{
				size: componentSize,
			}}
			size={componentSize}
			labelAlign="left"
		>
			<Form.Item label="Receiver">
				<Input
					placeholder="Enter receiver address"
					value={address}
					onChange={(e) => setAddress(e.target.value)}
				/>
			</Form.Item>
			<Form.Item label="Amount">
				<InputNumber
					min="0"
					style={{ width: '100%' }}
					value={amount}
					onChange={(e) => setAmount(e)}
					placeholder="Enter amount"
				/>
			</Form.Item>
			<Form.Item wrapperCol={{
				lg: { span: 14, offset: 3 },
				xl: { span: 14, offset: 2 },
				xxl: { span: 14, offset: 2 } }}
			>
				<Button type="primary" onClick={(e) => transferTokens(e)}>Transfer tokens</Button>
			</Form.Item>
		</Form>

	);
}

export default TransferForm;