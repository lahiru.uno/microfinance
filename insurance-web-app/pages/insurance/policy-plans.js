import { Row, Col } from 'antd';
import { useState } from 'react';
import CreatePlanForm from '../../components/plans/CreatePlanForm';
import PlansTable from '../../components/plans/PlansTable';

function Deals() {
  const [togglePlan, setTogglePlan] = useState(false);
  return (
    <Row gutter={[16, 16]}>
      <Col span={24}>
				<CreatePlanForm setTogglePlan={setTogglePlan} togglePlan={togglePlan} />
			</Col>
      <Col span={24}>
        <PlansTable togglePlan={togglePlan}></PlansTable>
      </Col>
    </Row>
  )
}

export default Deals;